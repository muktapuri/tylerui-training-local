﻿using System.Collections.Generic;
using System.Web;
using System.Web.Optimization;

namespace TylerUITrainingWebApp.Areas.TylerUITrainingWebApp
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/TylerUITrainingWebAppJs")
            .Include("~/Areas/TylerUITrainingWebApp/Scripts/app.js")
            .Include("~/Areas/TylerUITrainingWebApp/Scripts/controller.edit.js")
            .Include("~/Areas/TylerUITrainingWebApp/Scripts/controller.list.js")
            .Include("~/Areas/TylerUITrainingWebApp/Scripts/service.abc.js")
            );

        }
    }
    public class TylerUITrainingWebAppCacheModel : TylerUI.AppCacheModel
    {
        public override void AddAppResourcesToManifest()
        {
            AddBundle("~/bundles/TylerUITrainingWebAppJs");
            AddFiles("~/Areas/TylerUITrainingWebApp/Views/Index", "*.html");
            AddFiles("~/Areas/TylerUITrainingWebApp/Content/images", "*.svg");
        }
    }
}
