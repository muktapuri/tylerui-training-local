﻿angular.module('TylerUITrainingWebAppApp')
  .controller('ListCtrl', ['$scope', 'abcService', '$location', 'appConfig', function ($scope, abcService, $location, appConfig) {
    $scope.items = abcService.list();
    $scope.left = function () { return 100 - $scope.message.length; };
    $scope.clear = function () { $scope.message = ""; };
    $scope.save = function () { alert("Note Saved"); };
    $scope.edit = function (index) {
      $location.path(/edit/ + index);
    };
  }]);