﻿angular.module('TylerUITrainingWebAppApp')
  .service('abcService', ['$http', function ($http) {


    // these are test calls to services, showing the authorization context and server side authorization manager.
    //
    // some pass the authorization manager, and some are refused
    // the requests that are refused show toast with the http status code and description
    //
    // put breakpoints in the service project /Security/<YourApp>AuthorizationManager.cs to see 
    // how to use resource authorize attributes to run through the authorization manager for a controller
    //
    /*
    $http.get(appConfig.serviceUri + "Parties");
    $http.get(appConfig.serviceUri + "Parties(1)");
    $http.get(appConfig.serviceUri + "Parties(3)");
    $http.get(appConfig.serviceUri + "Parties(5)");
    $http.get(appConfig.serviceUri + "Cases");
    $http.get(appConfig.serviceUri + "Cases(1)");
    $http.get(appConfig.serviceUri + "Cases(3)");
    $http.get(appConfig.serviceUri + "Cases(5)");
     */

    var items = [
      { value: 'a', firstname: 'Apple', lastname: 'Acorn' },
      { value: 'b', firstname: 'Banana', lastname: 'Bulb' },
      { value: 'c', firstname: 'Citrus', lastname: 'Clover' },
      { value: 'd', firstname: 'Duran', lastname: 'Daisy' }
    ];

    this.list = function () {
      return items;
    };

    this.viewItem = function (index) {
      return items[index];
    };

    this.save = function (index, item) {
      items[index] = item;
    };
  }]);