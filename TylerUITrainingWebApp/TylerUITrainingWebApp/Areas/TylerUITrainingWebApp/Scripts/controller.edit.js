﻿angular.module('TylerUITrainingWebAppApp')
  .controller('EditCtrl', ['$scope', '$routeParams', '$location', 'ttNotificationService', 'abcService', 'ttLocale', 'appConfig', function ($scope, $routeParams, $location, ttNotificationService, abcService, ttLocale, appConfig) {
    $scope.item = angular.copy(abcService.viewItem($routeParams.id));

    $scope.save = function () {
      ttNotificationService.info(ttLocale.getString('Saved {{first}} {{last}}', { first: $scope.item.firstname, last: $scope.item.lastname }));
      abcService.save($routeParams.id, $scope.item);
      $location.path('/list/');
    };
    $scope.showDatePicker = function (ev) {
      $mdDatePicker(ev, $scope.date).then(function (selectedDate) {
        $scope.date = selectedDate;
      });
    }
    // calendar
    $scope.today = function () {
      $scope.dt = new Date();
    };
    $scope.today();
    $scope.clear = function () {
      $scope.dt = null;
    };
    $scope.route = function (path) {
      $location.path(path);
    };
    // Disable weekend selection
    $scope.disabled = function (date, mode) {
      return (mode === 'day' && (date.getDay() === 0 || date.getDay() === 6));
    };
    $scope.toggleMin = function () {
      $scope.minDate = $scope.minDate ? null : new Date();
    };
    $scope.toggleMin();
    $scope.open = function ($event) {
      $event.preventDefault();
      $event.stopPropagation();
      $scope.opened = true;
    };
    $scope.dateOptions = {
      formatYear: 'yy',
      startingDay: 1
    };
    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0];
  }]);