﻿angular.module("TylerUITrainingWebAppApp", ['ngAria', 'ngRoute', 'TylerUI', 'TylerUI.Components', 'gettext', 'ngScrollSpy'])
  .constant('appConfig', window.appConfig)
  .config(['$routeProvider', '$locationProvider', 'appConfig', function ($routeProvider, $locationProvider, appConfig) {
    $routeProvider
      .when('/list', { templateUrl: appConfig.virtualDirectory + 'Areas/TylerUITrainingWebApp/Views/Index/List.html', controller: 'ListCtrl' })
      .when('/edit/:id', { templateUrl: appConfig.virtualDirectory + 'Areas/TylerUITrainingWebApp/Views/Index/Edit.html', controller: 'EditCtrl' })
      .otherwise({ redirectTo: '/list' });
    $locationProvider.html5Mode(false);
  }])
  // https://docs.angularjs.org/guide/production
  .config(['$compileProvider', function ($compileProvider) {
    $compileProvider.debugInfoEnabled(false);
  }])
  .run(['appConfig', 'ttLocale', 'gettextCatalog', '$rootScope', function (appConfig, ttLocale, gettextCatalog, $rootScope) {    
    $rootScope.virtualDirectory = appConfig.virtualDirectory;
    // tt-footer
    $rootScope.footer = {};
    $rootScope.footer.copyRightDate = appConfig.copyRightDate;
    $rootScope.footer.version = appConfig.version;
    // utility
    $rootScope.hasValue = function (value) {
      return !!value && value !== '';
    };
    //
    ttLocale.init(gettextCatalog);
    ttLocale.setCurrentLanguage(appConfig.acceptLanguages);
    ttLocale.setDebug(false); //for debug, if true, prepend [MISSING]: to each untranslated string.
  }]);

// bootstrap the app
$(angular.element(document).ready(function () {
  angular.bootstrap(angular.element(app), ['TylerUITrainingWebAppApp'], {
    // https://docs.angularjs.org/guide/di#using-strict-dependency-injection
    strictDi: false
  });
}));

