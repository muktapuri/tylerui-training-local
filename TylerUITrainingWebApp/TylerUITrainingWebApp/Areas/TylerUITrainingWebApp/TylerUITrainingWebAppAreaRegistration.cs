﻿using System.Web.Mvc;
using System.Web.Optimization;

namespace TylerUITrainingWebApp.Areas.TylerUITrainingWebApp
{
    public class TylerUITrainingWebAppAreaRegistration : AreaRegistration
    {
        public override string AreaName
        {
            get
            {
                return "TylerUITrainingWebApp";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context)
        {
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
    }
}