angular.module('gettext').run(['gettextCatalog', function (gettextCatalog) {
/* jshint -W100 */
    gettextCatalog.setStrings('es', {"<i class=\"fa fa-info-circle\"></i>TylerUi Bootstrap Theme":"Información del tema","Cancel":"Cancelar","Edit":"Editar","Editing {{item.value}}":"Edición  {{item.value}}","First Name":"Nombre","First Name:<br><strong>{{i.firstname}}</strong>":"Apellido:<br><strong>{{i.lastname}}</strong>","Last Name":"Apellidos","Last Name:<br><strong>{{i.lastname}}</strong>":"Apellido:<br><strong>{{i.lastname}}</strong>","Save":"Guardar","Saved {{first}} {{last}}":"Guardado  {{first}} {{last}}","Simple List":"Lista simple","Value:<br><strong>{{i.value}}</strong>":"Valor:<br><strong>{{i.value}}</strong>"});
/* jshint +W100 */
}]);