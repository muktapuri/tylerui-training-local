﻿//https://github.com/auth0/angular-jwt/blob/master/dist/angular-jwt.js
//The MIT License (MIT)
angular.module('TylerUI')
  .service('jwtHelper', [function () {

    this.urlBase64Decode = function (str) {
      var output = str.replace('-', '+').replace('_', '/');
      switch (output.length % 4) {
        case 0: { break; }
        case 2: { output += '=='; break; }
        case 3: { output += '='; break; }
        default: {
          throw 'Illegal base64url string!';
        }
      }
      return window.atob(output); //polifyll https://github.com/davidchambers/Base64.js
    }


    this.decodeToken = function (token) {
      var parts = token.split('.');

      if (parts.length !== 3) {
        throw new Error('JWT must have 3 parts');
      }

      var decoded = this.urlBase64Decode(parts[1]);
      if (!decoded) {
        throw new Error('Cannot decode the token');
      }

      return JSON.parse(decoded);
    }

    this.getTokenExpirationDate = function (token) {
      var decoded;
      decoded = this.decodeToken(token);

      if (!decoded.exp) {
        return null;
      }

      var d = new Date(0); // The 0 here is the key, which sets the date to the epoch
      d.setUTCSeconds(decoded.exp);
      return d;
    };

    this.isTokenExpired = function (token) {
      if (angular.isUndefined(token) || token === "") {
        return false;
      }

      var d = this.getTokenExpirationDate(token);

      if (!d) {
        return false;
      }

      // Token expired?
      return !(d.valueOf() > new Date().valueOf());
    };
  }]);

// Add authentication header to all http requests.
angular.module('TylerUI').factory('httpTokenInterceptor', ['appConfig', '$q', '$window', 'jwtHelper', 'ttNotificationService',
  function (appConfig, $q, $window, jwtHelper, ttNotificationService) {
    var interceptor = {};

    interceptor.request = function (config) {
      // Reload if token is expired (web session should be expired also)
      if (jwtHelper.isTokenExpired(appConfig.token)) {
        $window.location.assign(appConfig.virtualDirectory);
      }
      // Add header 
      config.headers.Authorization = 'Bearer ' + appConfig.token;
      return config;
    };

    interceptor.response = function (response) {
      return response || $q.when(response);
    };
    interceptor.responseError = function (rejection) {
      //fixme language on messages
      var msg = rejection.statusText || "Request Failed";
      //console.log("httpTokenInterceptor responseError()")
      //console.log(rejection);
      if (rejection.status === 401) {
        ttNotificationService.error("Unauthorized");
      } else if (rejection.status === 403) {
        ttNotificationService.error("Forbidden");
      } else if (rejection.status === 500) {
        //if there is an actual error message show that
        if ((rejection.data != undefined || rejection.data != null) &&
            (rejection.data.error != undefined || rejection.data.error != null) &&
            (rejection.data.error.innererror != undefined || rejection.data.error.innererror != null)) {
          ttNotificationService.error(rejection.data.error.innererror.message);
        }
        else {
          ttNotificationService.error("Server Error");
        }
      } else {
        if (rejection.statusText.length > 0) {
          ttNotificationService.error(rejection.statusText);
        }
        else {
          ttNotificationService.error("Error " + rejection.status);
        }
      }
      return $q.reject(rejection);
    };

    return interceptor;
  }]).config(['$httpProvider', function ($httpProvider) {
    $httpProvider.interceptors.push('httpTokenInterceptor');
  }]);
