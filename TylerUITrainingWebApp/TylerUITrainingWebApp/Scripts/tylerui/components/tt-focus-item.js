﻿angular.module('TylerUI.Components')
.directive('ttFocusItem', ['$timeout', function ($timeout) {
  return {
    require: '^ttFocusContainer',
    link: function ($scope, elem, attrs, ttFocusContainerCtrl) {
      // register element
      $timeout(function () {
        ttFocusContainerCtrl.registerElement(elem[0], attrs.ttFocusOrder);
      });

      elem.on('click', function () {
        ttFocusContainerCtrl.onItemClicked(elem[0]);
        ttFocusContainerCtrl.updateBounds();
      })

      // unregister element
      $scope.$on('$destroy', function () {
        ttFocusContainerCtrl.unregisterElement(elem[0]);
      })
    }
  };
}]);