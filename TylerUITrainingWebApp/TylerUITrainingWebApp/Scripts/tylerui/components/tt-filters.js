﻿angular.module('TylerUI.Components')

.filter('splitBindingFilter', [function () {
  return function (value, item) {
    var args = value.split('.');

    var count = args.length;
    if (count > 1) {
      // we have child binding {{item.value.childvalue}}
      var temp = item;
      for (var i = 0; i < count; i++) {
          temp = temp[args[i]];
      }
      return temp;
    } else {
      // we only have one binding {{item.value}}
      return item[args[0]];
    }
  }
}])

.filter('applyFilter', ['$filter', function ($filter) {
  return function (value, filterSpec) {
    if (!filterSpec) {
      return value;
    }
    var args = filterSpec.split('|');
    var filter = $filter(args.shift());
    args.unshift(value);
    return filter.apply(null, args);
  };
}]);