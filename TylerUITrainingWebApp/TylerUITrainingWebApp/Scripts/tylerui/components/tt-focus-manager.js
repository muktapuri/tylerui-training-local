﻿angular.module('TylerUI.Components')
.service('ttFocusManager', ['$log', function ($log) {
  var groups = [],
      selectedGroup = null,
      primaryGroup = null;
  function hasValue(value) {
    return !!value && value !== '';
  }

  return {
    next: function () {
      if (hasValue(groups) && groups.length > 0) {
        if (!hasValue(selectedGroup)) {
          selectedGroup = groups[0];
        }
        selectedGroup.next();
      } else {
        $log.error('ttFocusManager.next(), groups have not been set, got:', groups);
      }
    },
    nextPrimary: function () {
      if (hasValue(groups) && groups.length > 0) {
        if (hasValue(primaryGroup)) {
          if (primaryGroup !== selectedGroup) {
            selectedGroup.clear();
            selectedGroup = primaryGroup;
          }
        } else {
          $log.info('ttFocusManager.next(), primaryGroup is not set, will default to the current group. got:', selectedGroup);
        }
        if (selectedGroup === null) {
          selectedGroup = groups[0];
        }
        selectedGroup.next();
      } else {
        $log.error('ttFocusManager.next(), groups have not been set, got:', groups);
      }
    },
    prev: function () {
      if (hasValue(groups) && groups.length > 0) {
        if (!hasValue(selectedGroup)) {
          selectedGroup = groups[0];
        }
        selectedGroup.prev();
      } else {
        $log.error('ttFocusManager.next(), groups have not been set, got:', groups);
      }
    },
    prevPrimary: function () {
      if (hasValue(groups) && groups.length > 0) {
        if (hasValue(primaryGroup)) {
          if(primaryGroup !== selectedGroup){
            selectedGroup.clear();
            selectedGroup = primaryGroup;
          }
        } else {
          $log.info('ttFocusManager.next(), primaryGroup is not set, will default to the current group. got:', selectedGroup);
        }
        if (selectedGroup === null) {
          selectedGroup = groups[0];
        }
        selectedGroup.prev();
      } else {
        $log.error('ttFocusManager.next(), groups have not been set, got:', groups);
      }
    },
    registerGroup: function (focusGroup) {
      groups.push(focusGroup);
      if (focusGroup.isPrimary) {
        primaryGroup = focusGroup;
        selectedGroup = primaryGroup;
      }
    },
    setSelectedGroup: function (id) {
      if (hasValue(id)) {
        if (hasValue(groups) && groups.length > 0) {
          var count = groups.length;
          for (var i = 0; i < count; i++) {
            if (groups[i].getID() === id) {
              if (hasValue(selectedGroup)) {
                if (selectedGroup.getID() !== id) {
                  selectedGroup.clear();
                  selectedGroup = groups[i];
                  selectedGroup.goToSelected();
                }
              } else {
                selectedGroup = groups[i];
                selectedGroup.goToSelected();
              }
              break;
            }
          }
        } else {
          $log.error('ttFocusManager.setSelectedGroup(), groups have not been set, got:', groups);
        }
      }
    },
    unregisterGroup: function (focusGroup) {
      var count = groups.length;
      for (var i = 0; i < count; i++) {
        var group = groups[i];
        if (group.element = focusGroup.element) {
          groups.splice(i, 1);
          break;
        }
      }
    }
  };
}]);