﻿angular.module('TylerUI')
.config(['$mdThemingProvider', function ($mdThemingProvider) {
  $mdThemingProvider.definePalette('tyler-primary', {
    '50': 'ECEEF4',
    '100': 'C6CBDD',
    '200': 'A0A9C7',
    '300': '808CB4',
    '400': '606FA2',
    '500': '40528F',
    '600': '38487D',
    '700': '303E6B',
    '800': '283359',
    '900': '202948',
    'A100': 'C6CBDD',
    'A200': 'A0A9C7',
    'A400': '606FA2',
    'A700': '303E6B',
    'contrastDefaultColor': 'light',
    'contrastDarkColors': ['50', '100', '200', '300', '400', 'A100'],
    'contrastLightColors': undefined
  });
  $mdThemingProvider.definePalette('tyler-primary-accent', {
    '50': '#c5ce96',
    '100': '#bcc585',
    '200': '#b2bd73',
    '300': '#a9b562',
    '400': '#9fac51',
    '500': '#8F9B49',
    '600': '#7f8a41',
    '700': '#6f7839',
    '800': '#5f6731',
    '900': '#4f5628',
    'A100': '#cfd6a7',
    'A200': '#d8deb9',
    'A400': '#e2e6ca',
    'A700': '#3f4420',
    'contrastDefaultColor': 'light',
    'contrastDarkColors': ['50', '100', '200', '300', '400', 'A100'],
    'contrastLightColors': undefined
  });
  $mdThemingProvider.definePalette('tyler-secondary', {
    '50': '#ffffff',
    '100': '#ffffff',
    '200': '#ffffff',
    '300': '#ffffff',
    '400': '#f6f6f6',
    '500': '#e9e9e9',
    '600': '#dcdcdc',
    '700': '#cfcfcf',
    '800': '#c3c3c3',
    '900': '#b6b6b6',
    'A100': '#ffffff',
    'A200': '#ffffff',
    'A400': '#ffffff',
    'A700': '#a9a9a9',
    'contrastDefaultColor': 'light',
    'contrastDarkColors': ['50', '100', '200', '300', '400', '500', '600', '700', '800', '900', 'A100', 'A200', 'A400', 'A700'],
    'contrastLightColors': undefined
  });
  $mdThemingProvider.definePalette('tyler-secondary-accent', {
    '50': 'rgba(0, 0, 0, 0.001)',
    '100': 'rgba(0, 0, 0, 0.001)',
    '200': 'rgba(0, 0, 0, 0.001)',
    '300': 'rgba(0, 0, 0, 0.001)',
    '400': 'rgba(0, 0, 0, 0.001)',
    '500': 'rgba(0, 0, 0, 0.001)',
    '600': 'rgba(0, 0, 0, 0.001)',
    '700': 'rgba(0, 0, 0, 0.001)',
    '800': 'rgba(0, 0, 0, 0.001)',
    '900': 'rgba(0, 0, 0, 0.001)',
    'A100': 'rgba(0, 0, 0, 0.001)',
    'A200': 'rgba(0, 0, 0, 0.001)',
    'A400': 'rgba(0, 0, 0, 0.001)',
    'A700': 'rgba(0, 0, 0, 0.001)',
    'contrastDefaultColor': 'light',
    'contrastDarkColors': ['50', '100', '200', '300', '400', '500', '600', '700', '800', '900', 'A100', 'A200', 'A400', 'A700'],
    'contrastLightColors': undefined
  });
  /*
  'grey': {
  '50': '#fafafa',
  '100': '#f5f5f5',
  '200': '#eeeeee',
  '300': '#e0e0e0',
  '400': '#bdbdbd',
  '500': '#9e9e9e',
  '600': '#757575',
  '700': '#616161',
  '800': '#424242',
  '900': '#212121',
  '1000': '#000000',
  'A100': '#ffffff',
  'A200': '#eeeeee',
  'A400': '#bdbdbd',
  'A700': '#616161',
  'contrastDefaultColor': 'dark',
  'contrastLightColors': '600 700 800 900'
},
*/
  var tylerGray = $mdThemingProvider.extendPalette('grey', {
    //does all md-content and overrides md-content-main (later style)
    // so card and backgrounds on main and footer are all gray
    //'A100': '#e9e9e9',
    'contrastDefaultColor': 'dark',
    'contrastLightColors': '600 700 800 900',
  });
  $mdThemingProvider.definePalette('tyler-grey', tylerGray);

  $mdThemingProvider.theme('tyler-default-theme')
  .primaryPalette('tyler-primary')
  .backgroundPalette('tyler-grey')
  .accentPalette('tyler-primary-accent', {
    'default': '500',
    'hue-1': '300',
    'hue-2': '800',
    'hue-3': 'A100'
  });

  $mdThemingProvider.theme('tyler-secondary-theme')
  .primaryPalette('tyler-secondary')
  .backgroundPalette('tyler-grey')
  .accentPalette('tyler-secondary-accent');

  $mdThemingProvider.theme('tyler-dark-theme')
  .primaryPalette('tyler-primary')
  .accentPalette('tyler-primary-accent')
  .backgroundPalette('tyler-primary', {
    'default': '500',
    'hue-1': '300',
    'hue-2': '800',
    'hue-3': 'A100'
  })
  .dark();

  $mdThemingProvider.theme('tyler-secondary-theme')
  .primaryPalette('tyler-secondary')
  .accentPalette('tyler-secondary-accent')
  .backgroundPalette('tyler-grey')

  $mdThemingProvider.setDefaultTheme('tyler-default-theme');
}]);