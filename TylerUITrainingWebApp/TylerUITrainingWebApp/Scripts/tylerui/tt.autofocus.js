﻿// focus on input after dom is ready and handle focus for angular show hide
angular.module('TylerUI').directive('ttAutofocus', ['$timeout', '$parse', function ($timeout, $parse) {
	return {
		link: function (scope, element, attrs) {
			scope.$watch(attrs.ttAutofocus, function (value) {
				if (value === undefined || value === true) {
					$timeout(function () {
						element[0].focus();
						element[0].select();
					}, 0);
				}
			});
		}
	};
}]);