﻿using System.Web.Http;
using System.Web.OData.Builder;
using System.Web.OData.Extensions;
using Owin;
using Tyler.WebApp.Security.Web;
using TylerUITrainingWebAppService.Models;

namespace TylerUITrainingWebAppService
{
    public partial class Startup
    {
        // For more information on configuring authentication, please visit http://go.microsoft.com/fwlink/?LinkId=301864
        public void ConfigureWebApi(IAppBuilder app)
        {
            var config = new HttpConfiguration();

            // Web API routes
            config.MapHttpAttributeRoutes(); //NB Must come before OData route mapping
            config.Routes.MapHttpRoute("DefaultApi", "api/{controller}/{id}", new { id = RouteParameter.Optional });

            // OData
            ODataModelBuilder builder = new ODataConventionModelBuilder();
            builder.EntitySet<SimpleDataItem>("SimpleDataItems");
            builder.EntitySet<Recipe>("Recipes");
            builder.EntitySet<Case>("Cases");
            builder.EntitySet<Party>("Parties");
            config.MapODataServiceRoute("ODataRoute", null, builder.GetEdmModel());

            WebApiConfigBearerToken.Configure(config);

            app.UseWebApi(config);
        }
    }
}