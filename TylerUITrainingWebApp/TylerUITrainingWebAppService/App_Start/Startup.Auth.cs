﻿using System;
using System.Configuration;
using Microsoft.Owin;
using Owin;
using TylerUITrainingWebApp.Security;

namespace TylerUITrainingWebAppService
{
    public partial class Startup
    {
        // For more information on configuring authentication, please visit http://go.microsoft.com/fwlink/?LinkId=301864
        public void ConfigureAuth(IAppBuilder app)
        {
            app.UseResourceAuthorization(new TylerUITrainingWebAppAuthorization());
        }
    }
}