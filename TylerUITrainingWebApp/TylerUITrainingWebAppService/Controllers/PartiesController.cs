﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.OData;
using Thinktecture.IdentityModel.WebApi;
using Tyler.WebApp.Security.Web;
using TylerUITrainingWebApp.Security;
using TylerUITrainingWebAppService.Models;

namespace TylerUITrainingWebAppService.Controllers
{
    [ResourceAuthorize(TylerResources.Actions.View, TylerUITrainingWebAppResources.Parties)]
    public class PartiesController : ODataController
    {
        // GET  /Parties
        [EnableQuery]
        //[ResourceAuthorize(TylerResources.Actions.ViewAll, ExampleResources.Parties)]
        [AllowAnonymous]
        public IHttpActionResult Get()
        {
            return NotFound();
        }

        // GET  /Parties({id})
        [EnableQuery]
        [ResourceAuthorize(TylerResources.Actions.View, TylerUITrainingWebAppResources.Parties)]
        public SingleResult<Party> Get([FromODataUri] int key)
        {
            //CheckAccessInAdditionToResourceAuthorizeAttribute(key);

            var result = new List<Party>
      {
        new Party
        {
          PartyID = key,
          FirstName = "Bob",
          LastName = "Smith"
        }
      }.AsQueryable();
            return SingleResult.Create(result);
        }

        // POST  /Parties
        [ResourceAuthorize(TylerResources.Actions.Add, TylerUITrainingWebAppResources.Parties)]
        public async Task<IHttpActionResult> Post([FromBody] Party p)
        {
            p.PartyID = 75371;
            // return the newly saved item
            return Created(p);
        }

        private void CheckAccessInAdditionToResourceAuthorizeAttribute(int key)
        {
            // if annoations (Action, Resource, Key Value) does not provide 
            // enough info for the AuthorizationDecisions, 
            // check access can be called with additional action / resource/ data 
            // using the Request Extension
            // This Resource data is stored by index and 
            // ResourceAuthorizationContext.TryGetContextDataByIndex(1, out value)
            // can be used to retrieve it
            if (!Request.CheckAccess(
              TylerResources.Actions.Edit,
              TylerUITrainingWebAppResources.Parties,
              key.ToString(),
              "Some Other Info etc..."
              ))
            {
                throw new HttpResponseException(HttpStatusCode.Forbidden);
            }
        }
    }
}