﻿using Tyler.WebApp.Security.Web;

namespace TylerUITrainingWebApp.Security
{
    public class TylerUITrainingWebAppResources : TylerResources
    {
        public const string Recipes = "Recipes";
        public const string Reviews = "Reviews";
        public const string SimpleDataItems = "SimpleDataItems";
        public const string Parties = "Parties";
    }
}