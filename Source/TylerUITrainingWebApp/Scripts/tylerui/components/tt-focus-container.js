﻿angular.module('TylerUI.Components')
.directive('ttFocusContainer', ['ttFocusManager', '$log', '$timeout', function (ttFocusManager, $log, $timeout) {
  var containerCtrl = ['$scope', '$element', '$attrs', function ($scope, $element, $attrs) {
    // private
    var eventEndOfContainer = null,
        eventStartOfContainer = null,
        identifier = '[tt-focus-container]',
        onContainerClicked = null,
        selectedItem = null;
    function getIsEnd(items) {
      var i = items.indexOf(selectedItem);
      var count = items.length - 1;
      var isLastItem = i >= count;
      return isLastItem;
    }
    function getIsStart(items) {
      var i = items.indexOf(selectedItem);
      var isFirstItem = i <= 0;
      return isFirstItem;
    }
    function hasValue(value) {
      return !!value && value !== '';
    }
    function toArray(x) {
      for (var i = 0, a = []; i < x.length; i++) {
        a.push(x[i]);
      }
      return a;
    }

    // public
    this.isEnd = false;
    this.isStart = false;
    this.items = [];
    this.clear = function () {
      selectedItem = null;
    }
    this.element = $element;

    this.first = function () {
      if (hasValue(this.items) && this.items.length > 0) {
        selectedItem = this.items[0];
        this.updateBounds();
      }
    }
    this.getSelectedItem = function () {
      return selectedItem;
    }
    this.initialize = function (options) {
      this.element = options.elem;
      onContainerClicked = options.onContainerClicked;
      eventEndOfContainer = $scope.$eval(options.attrs.ttOnEndOfContainer);
      eventStartOfContainer = $scope.$eval(options.attrs.ttOnStartOfContainer);
    }
    this.last = function () {
      if (hasValue(this.items) && this.items.length > 0) {
        selectedItem = this.items[this.items.length - 1];
        this.updateBounds();
      }
    }
    this.next = function () {
      if (hasValue(selectedItem)) {
        // in the middle of the container
        var nextItem = this.items[this.items.indexOf(selectedItem) + 1];
        selectedItem = nextItem;
      } else {
        // selectedItem is null so select the first one.
        selectedItem = this.items[0];
      }
      this.updateBounds();
    }
    this.onEndOfContainer = function () {
      if (hasValue(eventEndOfContainer)) {
        eventEndOfContainer();
      }
    }
    this.onStartOfContainer = function () {
      if (hasValue(eventStartOfContainer)) {
        eventStartOfContainer();
      }
    }
    this.prev = function () {
      if (hasValue(selectedItem)) {
        // in the middle of the container
        var prevItem = this.items[this.items.indexOf(selectedItem) - 1];
        selectedItem = prevItem;
      } else {
        // selectedItem is null so select the first one.
        selectedItem = this.items[0];
      }
      this.updateBounds();
    }
    this.registerElement = function (element, order) {
      var newItem,
          itemCount;
      newItem = { element: element, order: order || 0 };
      itemCount = this.items.length;
      if (itemCount > 0) {
        for (var i = 0; i < itemCount; i++) {
          if (newItem.order < this.items[i].order) {
            this.items.splice(i, 0, newItem);
          } else if (i == itemCount - 1) {
            this.items.push(newItem);
          }
        }
      } else {
        this.items.push(newItem);
      }
    }
    this.onItemClicked = function (element) {
      if (hasValue(element)) {
        var focusItem;
        for (var i = 0; i < this.items.length; i++) {
          if (this.items[i].element === element) {
            focusItem = this.items[i];
            break;
          }
        }
        if (hasValue(focusItem)) {
          selectedItem = focusItem;
          onContainerClicked(this);
        } else {
          $log.error(identifier, 'onItemClicked(item), item not found in items, item:', item);
        }
      }
    }
    this.unregisterElement = function (element) {
      var count = this.items.length;
      for (var i = 0; i < count; i++) {
        var item = this.items[i];
        if (item.element === element) {
          if (hasValue(selectedItem) && element === selectedItem.element) {
            selectedItem = null;
          }
          this.items.splice(i, 1);
          break;
        }
      }
    }
    this.updateBounds = function () {
      this.isEnd = getIsEnd(this.items);
      this.isStart = getIsStart(this.items);
    }
  }];

  return {
    controller: containerCtrl,
    scope: true,
    require: ['ttFocusContainer', '^ttFocusGroup'],
    link: function ($scope, elem, attrs, controllers) {
      var ttFocusContainer = controllers[0];
      var ttFocusGroup = controllers[1];

      ttFocusContainer.initialize({
        onContainerClicked: ttFocusGroup.onContainerClicked,
        elem: elem[0],
        attrs: attrs
      });

      // register container
      $timeout(function () {
        ttFocusGroup.registerContainer(ttFocusContainer, attrs.ttFocusOrder);
      });

      // unregister container
      $scope.$on('$destroy', function () {
        ttFocusGroup.unregisterContainer(ttFocusContainer);
      })
    }
  };
}]);