﻿angular.module('TylerUI.Components')

  /* 
  // Example Usage

  // --------------------------------------------------------------- //  
  // tt-label-value

  // html

  //notice label is optional
  <tt-label-value value="{{myData.mySection}}"  
                  css-class="{{bsCols.header}}"></tt-label-value>

  <tt-label-value label="Date"
                  value="{{myData.myDate | date: 'MM/dd/yyyy'}}"></tt-label-value
                  css-class="{{bsCols.standard}}">

  <tt-label-value label="Description"
                  value="{{myData.myDescription | truncate:10:'...'}}"
                  css-class="{{bsCols.standard}}"></tt-label-value>
  
  // angular controller
  $scope.myData = {};
  $scope.myData.mySection = "My Section";
  $scope.myData.myDate = "1980-01-01T17:00:00.000Z";
  $scope.myData.myDescription = "This is a summary with a lot of text";

  $scope.bsCols = {};

  // example of applying common bootstrap columns as a variable
  $scope.bsCols.header = "col-xs-12 col-sm-12 col-md-12 col-lg-12";
  $scope.bsCols.standard = "col-xs-6 col-sm-4 col-md-3 col-lg-2"; 

  // --------------------------------------------------------------- //
  // tt-label-value-group

  // html
  <tt-label-value-group label="Charges"
                        items="myData.myCharges"
                        fields="[
                        {name: 'count'},
                        {name: 'description', filter: 'truncate|120|...'},
                        {name: 'degree'},
                        {name: 'offenseDate', filter: 'date|MM/dd/yyyy h:mma'},
                        {name: 'amount', filter: 'currency'}
                        ]"
                        css-class="col-xs-6 col-sm-4 col-md-3 col-lg-2"></tt-label-value-group>

  // angular controller
  $scope.myData = {};
  $scope.myData.myCharges = [
                       {
                         "count":"1",
                         "description": "Assault",
                         "degree": "1st",
                         "offenseDate": "2007-01-01T17:00:00.000Z",
                         "amount": "6500.00"
                       }
                     ];

  // Special Notes:  

  // when you specify the filter its important to note, you have to separate the arguments by '|' instead of ':' 
  // because some strings such as h:mma are legit, and we separate args in the string by:    
  // var args = filterSpec.split('|');.
  // this has obvious downfalls when trying to use '|' for a legit string.
  // however at the time of writing this I haven't solved that problem.

  // item template
  <span class="bold" ng-repeat="field in fields">{{item[field.name] | applyFilter: field.filter}}  <br ng-if="$index >= fields.length -1"/></span> 

  // normal angular binding:                                        {{offenseDate | date:'MM/dd/yyyy h:mma'}}
  // in the fields="" attribute you would specify it like this:     fields="[{name: 'offenseDate', filter: 'date|MM/dd/yyyy h:mma'}]"
  
  // no spaces around '|'
  // you do not need qoutes around arguements, notice MM/dd/yyyy

  // --------------------------------------------------------------- //
  */

.directive('ttLabelValue', ['appConfig', function (appConfig) {
  function link($scope, $element, $attrs) {
    $scope.getContentUrl = function () {
      if ($scope.template != null && $scope.template != undefined && $scope.template != '') {
        return $scope.template;
      }
      else if (appConfig.virtualDirectory === undefined) {
        return '/Scripts/tylerui/components/tt-label-value.html';
      } else {
        return appConfig.virtualDirectory + 'Scripts/tylerui/components/tt-label-value.html';
      }
    };
  };

  return {
    restrict: 'E',
    template: '<div ng-include="getContentUrl()"></div>',
    controller: ['$scope', function ($scope) {
      $scope.hasValue = function (value) {
        return !!value && value !== '';
      };
    }],
    scope: {
      cssClass: '@', // should be used to apply layout related css classes
      label: '@',
      template: '=?',
      value: '@'
    },
    link: link
  };
}])

.directive('ttLabelValueGroup', ['appConfig', function (appConfig) {
  function link($scope, $element, $attrs) {
    $scope.getContentUrl = function () {
      if ($scope.template != null && $scope.template != undefined && $scope.template != '') {
        return $scope.template;
      }
      else if (appConfig.virtualDirectory === undefined) {
        return '/Scripts/tylerui/components/tt-label-value-group.html';
      } else {
        return appConfig.virtualDirectory + 'Scripts/tylerui/components/tt-label-value-group.html';
      }
    };
  };

  return {
    restrict: 'E',
    template: '<div ng-include="getContentUrl()"></div>',
    controller: ['$scope', 'appConfig', function ($scope, appConfig) {
      $scope.hasValue = function (value) {
        return !!value && value !== '';
      };

      $scope.limit = $scope.limit || 0;

      $scope.showItem = function (index) {
        if ($scope.limit > 0) {
          return index < $scope.limit;
        } else {
          return true;
        }
      };

      $scope.getItemContentUrl = function () {
        if ($scope.hasValue($scope.itemTemplate)) {
          return $scope.itemTemplate;
        }
        else if (appConfig.virtualDirectory === undefined) {
          return '/Scripts/tylerui/components/tt-label-value-group-item.html';
        } else {
          return appConfig.virtualDirectory + 'Scripts/tylerui/components/tt-label-value-group-item.html';
        }
      };
    }],
    scope: {
      cssClass: '@',      // add css classes
      items: '=',
      itemTemplate: '=?',
      fields: '=',        // properties used for bindings
      label: '@',
      limit: '=?',        // limit the amount of items that show in the group
      template: '=?'
    },
    link: link
  };
}]);