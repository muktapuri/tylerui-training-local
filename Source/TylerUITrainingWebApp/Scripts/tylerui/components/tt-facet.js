﻿angular.module('TylerUI.Components')
.directive('ttFacet', ['appConfig', '$timeout', function (appConfig, $timeout) {
  function compile($element, $attrs) {
    var expression,
        match,
        itemsExpression;

    expression = $attrs.ttItems;
    match = expression.match(/^([\s\S]+?)(?:\s+as\s+([\s\S]+?))?(?:\s+track\s+by\s+([\s\S]+?))?\s*$/);
    //match = expression.match(/^\s*([\s\S]+?)\s+in\s+([\s\S]+?)(?:\s+as\s+([\s\S]+?))?(?:\s+track\s+by\s+([\s\S]+?))?\s*$/);
    itemsExpression = match[1]; //collection
    //alias match[2]
    //track by match[3]

    var ttArgs = $attrs.ttArgs,
        ttAriaLabel = $attrs.ttAriaLabel,
        ttClearEvent = $attrs.ttClearEvent,
        ttClickEvent = $attrs.ttClickEvent,
        ttFields = $attrs.ttFields,
        ttHeader = $attrs.ttHeader,
        ttLessLabel = $attrs.ttLessLabel,
        ttMoreLabel = $attrs.ttMoreLabel,
        ttMultiselect = $attrs.ttMultiselect,
        ttTemplate = $attrs.ttTemplate;

    return function linker($scope) {
      $scope.$watchCollection(itemsExpression, function (collection) {
        $scope.items = collection;
      });
      $scope.$watch(ttArgs, function (value) {
        $scope.ttArgs = value;
      });
      $scope.ttAriaLabel = ttAriaLabel;
      $scope.$watch(ttClearEvent, function (value) {
        $scope.ttClearEvent = value;
      });
      $scope.$watch(ttClickEvent, function (value) {
        $scope.ttClickEvent = value;
      });
      $scope.$watchCollection(ttFields, function (collection) {
        $scope.ttFields = collection;
      });
      $scope.ttHeader = ttHeader;
      $scope.ttLessLabel = ttLessLabel;
      $scope.ttMoreLabel = ttMoreLabel;
      $scope.$watch(ttMultiselect, function (value) {
        $scope.ttMultiselect = value;
      });
      $scope.$watch(ttTemplate, function (value) {
        $scope.ttTemplate = value;
      });
      $scope.getContentUrl = function () {
        if ($scope.ttTemplate != null && $scope.ttTemplate != undefined && $scope.ttTemplate != '') {
          return $scope.ttTemplate;
        }
        else if (appConfig.virtualDirectory === undefined) {
          return '/Scripts/tylerui/components/tt-facet.html';
        } else {
          return appConfig.virtualDirectory + 'Scripts/tylerui/components/tt-facet.html';
        }
      }
    };
  };

  return {
    restrict: 'E',
    template: '<div ng-include="getContentUrl()"></div>',
    controller: ['$scope', 'appConfig', function ($scope, appConfig) {
      $scope.clearSelectedItem = function () {
        if (!$scope.hasValue($scope.ttMultiselect)) {
          $scope.selectedItem = null;
        }
      };
      $scope.hasValue = function (value) {
        return !!value && value !== '';
      };
      $scope.searchText = {};
      $scope.searchText.query = '';
      $scope.selectedItem = null;
      $scope.setSelectedItem = function (item) {
        if (!$scope.hasValue($scope.ttMultiselect)) {
          if (!$scope.hasValue($scope.selectedItem)) {
            $scope.selectedItem = item;
          } else if ($scope.selectedItem !== item) {
            $scope.selectedItem.selected = false;
            $scope.selectedItem = item;
          }
        }
      };
      $scope.ttLessLabel = $scope.ttLessLabel || 'Less';
      $scope.ttMoreLabel = $scope.ttMoreLabel || 'More';
      $scope.ttMultiselect = $scope.ttMultiselect || false;
      $scope.toggleMore = function () {
        $scope.isShowingMore = $scope.isShowingMore != null ? !$scope.isShowingMore : true;
      };
    }],
    scope: true,
    compile: compile
  };
}])
.filter('showSelected', function () {
  return function (list) {
    var i;
    var selectedList = [];
    var selectedItem;

    if (angular.isDefined(list) &&
            list.length > 0) {
      for (selectedItem = list[i = 0]; i < list.length; selectedItem = list[++i]) {
        if (selectedItem.selected == true)
          selectedList.push(selectedItem);
      }
    }
    return selectedList;
  };
})
.filter('hideSelected', function () {
  return function (list) {
    var i;
    var selectedList = [];
    var selectedItem;

    if (angular.isDefined(list) &&
            list.length > 0) {
      for (selectedItem = list[i = 0]; i < list.length; selectedItem = list[++i]) {
        if (!angular.isDefined(selectedItem.selected) || selectedItem.selected == false)
          selectedList.push(selectedItem);
      }
    }
    return selectedList;
  };
});