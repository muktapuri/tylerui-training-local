﻿var TylerODataKendoDataSourceConfig = {
    /*
    oDataServiceUrl- endpoint for oData service
    getKey - function to get the key from the data in a row
    kendoModelConfig - kendo model definition
    kendoColumnConfig - kendo columns definition  
    extendedKendoDataSourceConfig - optional parameter can be given that overrides or adds any of the standard properties in kendo data source, using angular.extend http://code.angularjs.org/1.2.16/docs/api/ng/function/angular.extend
    */
    extendKendoDataSource: function (oDataServiceUrl, getKey, kendoModelConfig, kendoColumnConfig, extendedKendoDataSourceConfig) {
        var oDataServiceUrlWithId = function (data) {
            return oDataServiceUrl + "(" + getKey(data) + ")";
        };
        var dataBound = function (e) {
            //console.log("dataBound");
        };
        var isDirtyDs = function (ds) {
            var dirty = false;
            $.each(ds._data, function () {
                if (this.dirty === true) {
                    dirty = true;
                }
            });
            return ds._destroyed.length > 0 || dirty;
        };
        var defaultGridDataSourceConfig = {
            type: "odata",
            transport: {
                create: {
                    url: oDataServiceUrl,
                    dataType: "json",
                    complete: function (jqXhr, textStatus) {
                        //console.log(textStatus, "create");
                    }
                },
                read: {
                    url: oDataServiceUrl,
                    dataType: "json",
                    complete: function (jqXhr, textStatus) {
                        //console.log(textStatus, "read");
                    }
                },
                update: {
                    url: oDataServiceUrlWithId,
                    dataType: "json",
                    complete: function (jqXhr, textStatus) {
                        //console.log(textStatus, "update");
                    }
                },
                destroy: {
                    url: oDataServiceUrlWithId,
                    dataType: "json",
                    complete: function (jqXhr, textStatus) {
                        //console.log(textStatus, "destroy");
                    }
                },
                parameterMap: function (data, operation) {
                    // remove extra json items, since odata controller is ignoring the MissingMemberHandling.Ignore;
                    delete data.guid;
                    delete data["odata.metadata"];
                    //console.log(data);
                    return kendo.data.transports["odata"].parameterMap(data, operation);
                }
            },
            schema: {
                // OData Insert returns individual attributes, so handle those also
                // data: function (data) { return data["value"]; },
                data: function (data) {
                    if (!data["value"]) {
                        data["value"] = [{}];
                        for (var field in this.model.fields)
                            data["value"][0][field] = data[field];
                    }
                    return data["value"];
                },
                total: function (data) {
                    return data["odata.count"];
                },
                model: kendoModelConfig
            },
            requestStart: function (e) {
                /*
                confirm navigation when there are pending changes
                need to filter out actions that start requests like delete, save, etc
                if (isDirtyDs(this)) {
                    e.preventDefault();
                    if (confirm('hasChanges')) {
                        //fixme nav data protection
                    }
                }
                */
            },
            error: function (e) {
                //should be overridden with an application specific error handler
                var xhr = e.xhr;
                var statusCode = e.status;
                var errorThrown = e.errorThrown;
                //console.log(xhr);
                //console.log(statusCode);
                //console.log(errorThrown);

                var xhrError = e.xhr.responseJSON["odata.error"];
                var message = xhrError.message.value;
                var innerMessage = xhrError.innererror.message;
                //console.log(message, "\n\n", innerMessage);
            },
            pageSize: 10,
            serverPaging: true,
            serverFiltering: true,
            serverSorting: true,
            batch: false,
            logic: "and"

        };
        angular.extend(defaultGridDataSourceConfig, extendedKendoDataSourceConfig);
        return new kendo.data.DataSource(defaultGridDataSourceConfig);
    }
};