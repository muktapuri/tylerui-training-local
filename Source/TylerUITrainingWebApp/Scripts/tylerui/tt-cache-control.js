var TylerUiCacheControl = {
  start: function (config) {
    var loadTime = Date.now();
    // refresh cache and reload app if needed
    appCacheNanny.start({
      checkInterval: 30 * 60000, // 30 minutes
      loaderPath: config.loaderPath,
    });
    appCacheNanny.on('updateready', function () {
      // if the app has been sitting here more than 20 seconds, ask
      // in other words, don't ask on initial load, there should be no work or state to protect from a reload
      if (Date.now() - loadTime > 20000) {
        var r = confirm(
          "Application Update\n\nAn application update is available for\n" +
          config.appTitle +
          ".\n\nWould you like to reload the applcation now?" +
          "\n\nWARNING: Any unsaved work will be lost."
          );
        if (r === true) {
          window.location.reload();
        }
      } else {
        window.location.reload();
      }
    });
  }
};
