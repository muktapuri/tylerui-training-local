﻿// Modified from the original directive.
// digitalfondue / df-tab-menu
// https://github.com/digitalfondue/df-tab-menu

// !!! Deprecated, use the tt-btn-group under TylerUI.Components

angular.module('TylerUI')
.directive('ttButtongroup', ['$window', '$timeout', 'appConfig', function ($window, $timeout, appConfig) {
  function updateItems($scope, $element, $attrs) {
    $scope.getContentUrl = function () {
      if ($scope.template != null && $scope.template != undefined && $scope.template != '') {
        return $scope.template;
      }
      else if (appConfig.virtualDirectory === undefined) {
        return '/Scripts/tylerui/ttbuttons/tt-buttongroup.html';
      } else {
        return appConfig.virtualDirectory + 'Scripts/tylerui/ttbuttons/tt-buttongroup.html';
      }
    };
    var root = $element[0];
    var doc = $window.document;
    var wdw = angular.element($window);
    $scope.dropdownOpen = false;

    var getElementsSize = function () {
      var elements = root.querySelectorAll('[button-item]');
      angular.element(elements).removeClass('ng-hide');
      var elementsSize = [];
      for (var e = 0; e < elements.length; e++) {
        var size = elements[e].offsetWidth;
        if (size > 0) {
          elementsSize[e] = elements[e].offsetWidth;
        }
      };
      return elementsSize;
    }

    var getMoreElementSize = function () {
      angular.element(root.querySelector('[more-item]')).removeClass('ng-hide');
      return root.querySelector('[more-item]').offsetWidth;
    }

    var getVisibleItems = function (_maxWidth) {
      var visibleItems = [];
      var elementsSize = getElementsSize();
      // we dont really need the scroll bar tolerance since we only use the global scrollbar.
      var sum = getMoreElementSize();

      //40px: scrollbar tolerance. Not proud of this, but it works...
      //var sum = getMoreElementSize() + 40;

      var items = root.querySelectorAll('[button-item]');
      for (var i = 0; i < items.length; i++) {
        sum += elementsSize[i];
        if (sum > _maxWidth) {
          return visibleItems;
        } else {
          visibleItems.push(i);
        }
      }
      return visibleItems;
    };

    var buildGroup = function () {
      var maxWidth = root.querySelector('[button-container]').offsetWidth;
      var visibleItems = getVisibleItems(maxWidth);
      var elements = root.querySelectorAll('[button-item]');
      var moreElements = root.querySelectorAll('[dropdown-item]');
      var moreMenuToggle = root.querySelector('[more-item]');

      if (visibleItems.length < root.querySelectorAll('[button-item]').length) {
        angular.element(moreMenuToggle).removeClass('ng-hide').attr('aria-hidden', 'false');

        for (var i = 0; i < elements.length; i++) {
          if (visibleItems.indexOf(i) != -1) {
            angular.element(elements[i]).removeClass('ng-hide').attr('aria-hidden', 'false');
            angular.element(moreElements[i]).addClass('ng-hide').attr('aria-hidden', 'true');
          } else {
            angular.element(elements[i]).addClass('ng-hide').attr('aria-hidden', 'true');
            angular.element(moreElements[i]).removeClass('ng-hide').attr('aria-hidden', 'false');
          }
        }
      } else {
        angular.element(moreMenuToggle).addClass('ng-hide').attr('aria-hidden', 'true');

        angular.element(elements).removeClass('ng-hide').attr('aria-hidden', 'false');

        $scope.dropdownOpen = false;
        drawDropDown();
      }
      angular.element(elements[elements.length - 1]).addClass('last-child');
    };

    var closeDropdown = function (e) {
      $scope.dropdownOpen = false;
      drawDropDown(e);
    };

    var drawDropDown = function () {
      if ($scope.dropdownOpen) {
        angular.element(root.querySelector('[more-item] [dropdown-toggle]')).addClass('open').attr({ 'aria-expanded': 'true' });
        angular.element(doc).bind('click', closeDropdown);
      } else {
        angular.element(root.querySelector('[more-item] [dropdown-toggle]')).removeClass('open').attr({ 'aria-expanded': 'false' });;
        angular.element(doc).unbind('click', closeDropdown);
      }
    };

    //dropdown controls
    var toggleDropdown = function (e) {
      if (e) { e.stopPropagation() };
      $scope.dropdownOpen = !$scope.dropdownOpen;
      drawDropDown();
    };

    angular.element(root.querySelector('[more-item] [dropdown-toggle]')).bind('click', toggleDropdown);

    wdw.bind('resize', buildGroup);

    $scope.$on('$destroy', function () {
      wdw.unbind('resize', buildGroup);
      angular.element(root.querySelector('[more-item] [dropdown-toggle]')).unbind('click', toggleDropdown);
      angular.element(doc).unbind('click', closeDropdown);
    });

    var buildGroupTimeout;
    $scope.$watch(function () {
      $timeout.cancel(buildGroupTimeout);
      buildGroupTimeout = $timeout(function () {
        buildGroup();
      }, 25, false);
    });
  };

  return {
    restrict: 'E',
    template: '<div ng-include="getContentUrl()"></div>',
    controller: ['$scope', 'appConfig', function ($scope, appConfig) {
      $scope.virtualDirectory = appConfig.virtualDirectory;
      $scope.toggleClicked = function () {
        console.log('toggle clicked');
      };
      $scope.dropdownOpen = false;
      $scope.alignButtonGroup = null;
    }],
    scope: {
      data: '=data',
      align: '=align',
      template: '=template'
    },
    link: function (scope, element, attrs) {
      scope.alignButtonGroup = scope.align == 'right' ? 'pull-right' : scope.align == 'left' ? 'pull-left' : '';
      updateItems(scope, element, attrs);
    }
  }
}]);