﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Common.Logging;

namespace TylerUITrainingWebApp.Areas.TylerUITrainingWebApp.Controllers
{
    [RouteArea("TylerUITrainingWebApp", AreaPrefix = "")]
    public class PingController : Controller
    {
        [Route("~/ping")]
        [AllowAnonymous]
        public ActionResult Index()
        {

            // if you can retry something, you can still log a warning exception
            //try
            //{
            //  throw new AppDomainUnloadedException("This is a warning exception");
            //}
            //catch (Exception e)
            //{
            //  LogManager.GetCurrentClassLogger().Warn(e);
            //}

            // Unhandled exceptions are automatically logged by elmah
            //var myStringArray = new String[3];
            //var x = myStringArray[7];

            return View();
        }
    }
}