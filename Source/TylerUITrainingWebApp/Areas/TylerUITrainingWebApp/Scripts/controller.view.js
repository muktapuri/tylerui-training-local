﻿angular.module('TylerUITrainingWebAppApp')
  .controller('ViewCtrl', ['$scope', '$routeParams', '$location', 'ttNotificationService', 'searchSvc', 'ttLocale', 'appConfig', function ($scope, $routeParams, $location, ttNotificationService, searchSvc, ttLocale, appConfig) {
      searchSvc.viewItem($routeParams.id)
        .success(function (personObject) {
            $scope.item = personObject;
        });

      $scope.templateUrl = $scope.virtualDirectory + 'Areas/TylerUITrainingWebApp/Views/Index/Templates/';

      $scope.handleIntent = function (intent) {
          window.open(intent.uri, "_blank");
      }
  }]);