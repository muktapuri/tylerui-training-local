﻿angular.module('customFilters', [])
  // A filter that will capitalize the first word of a string.
  // If the "all" flag is true, it will capitalize all the words in the string.
  .filter('capitalize', function () {
      return function (input, all) {
          // If the all parameter is true, capitalize first letter of every word. Otherwise only capitalize first letter of first word.
          // https://regex101.com/#javascript  -> good resource for debugging regex
          var reg = (all) ? /([^\W_]+[^\s-]*) */g : /([^\W_]+[^\s-]*)/;
          return (!!input) ? input.replace(reg, function (txt) { return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase(); }) : '';
      }
  })

  // A filter that will format a telephone number.
  // Supports USA and international formats
  .filter('tel', function () {
      return function (tel) {
          if (!tel) { return ''; }

          // If there is a '+' character at the start of the tel string, remove it.
          var value = tel.toString().trim().replace(/^\+/, '');

          if (value.match(/[^0-9]/)) {
              // If any character is not a number, do not do any conversion.
              return tel;
          }

          var country, city, number;

          switch (value.length) {
              case 10: // +1PPP####### -> C (PPP) ###-####
                  country = 1;
                  city = value.slice(0, 3);
                  number = value.slice(3);
                  break;

              case 11: // +CPPP####### -> CCC (PP) ###-####
                  country = value[0];
                  city = value.slice(1, 4);
                  number = value.slice(4);
                  break;

              case 12: // +CCCPP####### -> CCC (PP) ###-####
                  country = value.slice(0, 3);
                  city = value.slice(3, 5);
                  number = value.slice(5);
                  break;

              default:
                  return tel;
          }

          if (country == 1) {
              country = "";
          }

          number = number.slice(0, 3) + '-' + number.slice(3);

          return (country + " (" + city + ") " + number).trim();
      };
  });
