﻿angular.module('TylerUITrainingWebAppApp')
.service('searchSvc', ['$http', function ($http) {
    this.init = function () {
        // Send an http get request to the service to get the first 10 results of the search query.
        return $http.get(appConfig.serviceUri + "Search");
    };

    this.getPage = function (searchObject) {
        // Clear out the existing search results for the old page.
        // No need to send those back to the service when we expect a new set of results to be returned.
        searchObject.searchResult.hits = [];

        // The sorts and parameters collections are not nullable in the model.
        // Because we're not using them and we cannot just set them to null, we need to remove them so 
        // WebAPI can deserialize the search object we're sending it.
        delete searchObject.sorts;
        delete searchObject.parameters;

        // The OData model requires an id be specified. We're not using it in this app.
        // But we have to provide it since it is required.
        searchObject.id = "1";

        // We need to send the searchObject to the service this time because we need to specify the 'from'
        // value and the number of results to return. We cannot send parameters using get so we are using
        // post in this case.
        return $http.post(appConfig.serviceUri + "Search", angular.toJson(searchObject));
    };

    // Used to return the details of the person from the service.
    this.viewItem = function (personId) {
        return $http.get(appConfig.serviceUri + "People(" + personId + ")");
    };
}])