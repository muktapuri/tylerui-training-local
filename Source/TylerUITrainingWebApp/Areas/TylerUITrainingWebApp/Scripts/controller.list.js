﻿// Controller for page with list of people
angular.module('TylerUITrainingWebAppApp')
  .controller('ListCtrl', ['$scope', 'searchSvc', '$location', 'appConfig', function ($scope, searchSvc, $location, appConfig) {
      // Set values required for pagination
      $scope.resultsPerPage = 10;
      $scope.selectedPage = 1;
      $scope.totalPagedItems = 0;

      // This will be used to cache the object that stores the result that we get back 
      // from the TylerUITrainingWebAppService.
      $scope.cachedSearchObject = null;

      $scope.init = function () {
          searchSvc.init() // Get Result from service.
              .success(function (searchObject) {
                  // Cache the result. We will be using this to handle paging.
                  $scope.cachedSearchObject = searchObject;

                  // Save the JSON results into $scope.items so that we can use it in List.html
                  $scope.items = searchObject.searchResult.hits;

                  // To handle paging
                  this.setPagingCounts(searchObject);
              });
      };

      // Save the total number of results the query could return (the totalHits).
      // See how we are using $scope. We are storing it this way so we can later use it in List.html
      setPagingCounts = function (searchObject) {
          $scope.totalPagedItems = searchObject.searchResult.totalHits;
      }

      // Again, we are using $scope so that we can use this function on List.html
      // This function handles what happens when user clicks the numbers or arrows at the bottom of 
      // the list to change the page.
      $scope.onPaginationChange = function (page) {
          // Mark's magic formula to find the first person who will be displayed on the selected page.
          $scope.cachedSearchObject.from = (page - 1) * $scope.resultsPerPage;
          $scope.selectedPage = page;

          // Get the set of data for the page.
          $scope.getPage();
      }

      // Call the service to get the data for the page.
      $scope.getPage = function () {
          searchSvc.getPage($scope.cachedSearchObject)
              .success(function (searchObject) {
                  $scope.items = searchObject.searchResult.hits;
                  this.setPagingCounts(searchObject);
              });
      }

      // Handles the click of the view button.
      $scope.view = function (personId) {
          $location.path(/view/ + personId);
      };
  }]);