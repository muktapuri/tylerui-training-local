﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace TylerUITrainingWebApp
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            // must enable attribute routing for security nuget /signin /signout
            // This allows us to route using the [Route("~/<path>")] attribute on the .Net controllers.
            routes.MapMvcAttributeRoutes();
        }
    }
}
