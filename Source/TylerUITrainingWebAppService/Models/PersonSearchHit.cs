﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Tyler.WebApp.Search.Models;

namespace TylerUITrainingWebAppService.Models
{
    public class PersonSearchHit : SearchHit
    {
        public PersonSearchHit()
        {

        }

        public int id { get { return this.personId; } }
        public int personId { get; set; }
        public string lastName { get; set; }
        public string firstName { get; set; }
        public string middleName { get; set; }
    }
}