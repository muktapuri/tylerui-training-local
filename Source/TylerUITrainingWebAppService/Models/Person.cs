﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Tyler.WebApp.Infrastructure.Manifest.Model;

namespace TylerUITrainingWebAppService.Models
{
    public class Person
    {
        public Person()
        {
            this.emails = new List<Email>();
            this.intents = new List<Intent>();
        }

        public int id { get { return this.personId; } }
        public int personId { get; set; }
        public string lastName { get; set; }
        public string firstName { get; set; }
        public string middleName { get; set; }
        public string[] dateOfBirth { get; set; }
        public string race { get; set; }
        public string gender { get; set; }
        public string ethnicity { get; set; }
        public decimal annualIncome { get; set; }
        public string currentEmployer { get; set; }
        public string phoneNumber { get; set; }
        public string addressLine1 { get; set; }
        public string addressLine2 { get; set; }
        public string addressLine3 { get; set; }

        public List<Email> emails { get; set; }
        public List<Intent> intents { get; set; }
    }
}