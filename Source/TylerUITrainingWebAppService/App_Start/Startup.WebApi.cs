﻿using System.Web.Http;
using System.Web.OData.Builder;
using System.Web.OData.Extensions;
using Owin;
using Tyler.WebApp.Security.Web;
using TylerUITrainingWebAppService.Models;
using Tyler.WebApp.Search.Models;

namespace TylerUITrainingWebAppService
{
    public partial class Startup
    {
        // For more information on configuring authentication, please visit http://go.microsoft.com/fwlink/?LinkId=301864
        public void ConfigureWebApi(IAppBuilder app)
        {
            var config = new HttpConfiguration();

            // Web API routes
            config.MapHttpAttributeRoutes(); //NB Must come before OData route mapping
            config.Routes.MapHttpRoute("DefaultApi", "api/{controller}/{id}", new { id = RouteParameter.Optional });

            // OData
            var builder = new ODataConventionModelBuilder();
            builder.EnableLowerCamelCase(); // <-- affects PeopleController, but not SearchController

            builder.Ignore<PersonSearchHit>();

            builder.EntitySet<Person>("People"); // Create route to PeopleController
            builder.EntitySet<IndexSearch>("Search"); // Create route to SearchController

            config.MapODataServiceRoute("ODataRoute", null, builder.GetEdmModel());

            WebApiConfigBearerToken.Configure(config);

            app.UseWebApi(config);
        }
    }
}