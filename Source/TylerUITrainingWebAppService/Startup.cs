﻿using System;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(TylerUITrainingWebAppService.Startup))]
namespace TylerUITrainingWebAppService
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            ConfigureWebApi(app);
        }
    }
}