﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Tyler.WebApp.Search.Models;

namespace TylerUITrainingWebAppService
{
    public class ElasticSearchHelper
    {
        /// <summary>
        /// Returns a default search configuration object.
        /// </summary>
        /// <returns>Tyler.WebApp.Search.Models.IndexSearch</returns>
        public static IndexSearch ConstructDefaultIndexSearchObject()
        {
            return new IndexSearch()
            {
                Id = "1",
                SearchTimeMilliseconds = 0,
                From = 0,
                Size = 10,
                QueryString = string.Empty,
                Parameters = new AdditionalSearchParams()
            };
        }

        /// <summary>
        /// Returns a SearchDefinition object to be used when calling ElasticSearch via Nest.
        /// </summary>
        /// <returns>Tyler.WebApp.Search.Models.SearchDefinition</returns>
        public static SearchDefinition GetSearchDefinition()
        {
            return new SearchDefinition()
            {
                IndexName = WebConfigHelper.SiteId.ToLower() + "_trainingindex",
                IndexType = "person",
                FacetDefs = new List<FacetDefinitionBase>(),
                Sorts = new List<QuerySort>(),
                Filters = new List<QueryFilterBase>()
            };
        }
    }
}