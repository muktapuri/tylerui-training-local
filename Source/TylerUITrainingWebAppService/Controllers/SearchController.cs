﻿using Nest;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Results;
using System.Web.OData;
using Thinktecture.IdentityModel.WebApi;
using Tyler.WebApp.Search.Models;
using Tyler.WebApp.Security.Web;
using TylerUITrainingWebApp.Security;
using TylerUITrainingWebAppService.Models;

namespace TylerUITrainingWebAppService.Controllers
{
    public class SearchController : BaseController
    {

        [ResourceAuthorize(TylerResources.Actions.ViewAll, TylerUITrainingWebAppResources.People)]
        public IHttpActionResult Get()
        {
            var searchObject = new IndexSearch() { Id = "1", From = 0, Size = 10, Sorts = new List<QuerySort>() };
            return GetPeople(searchObject);
        }

        public IHttpActionResult Post(IndexSearch searchObject)
        {
            return GetPeople(searchObject);
        }

        private IHttpActionResult GetPeople(IndexSearch searchObject)
        {
            // --------------
            // Validate Input
            // --------------

            if (!base.ModelState.IsValid)
                return base.BadRequest(base.ModelState);

            if (searchObject == null)
                return base.BadRequest("No search object passed to search method.");

            // --------------------
            // Query Elastic-Search
            // --------------------

            ISearchResponse<Person> searchResponse = SearchClient.Search<Person>(selector => selector
                .Index(WebConfigHelper.IndexName)
                .From(searchObject.From)
                .Size(searchObject.Size)
                .MatchAll().SortAscending(person => person.personId));

            // ---------------------------------------------------
            // Transpose the search results into our result object
            // ---------------------------------------------------

            if ((searchResponse != null) && (searchResponse.Hits != null))
            {
                searchObject.SearchResult = new SearchResult();
                searchObject.SearchResult.TotalHits = searchResponse.HitsMetaData.Total;
                IEnumerable<Person> results = searchResponse.Hits.Select(hit => hit.Source);

                foreach (Person p in results)
                {
                    searchObject.SearchResult.Hits.Add(new PersonSearchHit()
                    {
                        personId = p.personId,
                        firstName = p.firstName,
                        lastName = p.lastName,
                        middleName = p.middleName
                    });
                }

                if (searchObject.SearchResult.Hits.Count > 0)
                {
                    return base.Json(searchObject, new Newtonsoft.Json.JsonSerializerSettings()
                    { ContractResolver = new CamelCasePropertyNamesContractResolver() });
                }
            }

            return base.BadRequest("Search result could not be loaded.");
        }

    }

}