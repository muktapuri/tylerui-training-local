﻿using Nest;
//using Newtonsoft.Json.Serialization; // TEMP (added for CamelCasePropertyNamesContractResolver)
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net; // TEMP (added for HttpStatusCode)
using System.Net.Http; // Temp (added for HttpResponseMessage)
//using System.Security.Claims;
//using System.Web;
using System.Web.Http;
//using System.Web.Http.Results; // TEMP (added for JsonResult)
using System.Web.OData;
using Thinktecture.IdentityModel.WebApi;
using Tyler.WebApp.Security.Web;
using TylerUITrainingWebApp.Security;
//using Tyler.WebApp.Security.Extensions;
using TylerUITrainingWebAppService.Models;

namespace TylerUITrainingWebAppService.Controllers
{
    public class PeopleController : BaseController
    {

        [EnableQuery]
        [ResourceAuthorize(TylerResources.Actions.View, TylerUITrainingWebAppResources.People)]
        public SingleResult<Person> Get([FromODataUri] int key)
        {
            // --------------------
            // Query Elastic-Search
            // --------------------

            ISearchResponse<Person> searchResponse = SearchClient.Search<Person>(
                s => s.Index(WebConfigHelper.IndexName).From(0).Size(1)
                .Query(q => q.Term(p => p.personId, key.ToString())));

            if ((searchResponse != null) && (searchResponse.Hits != null) && (searchResponse.Hits.Count() > 0))
            {

                Person person = searchResponse.Hits.First().Source;

                // ---------------
                // Resolve intents
                // ---------------

                // Future Enhancement...

                // ---------------------------
                // Return result as IQueriable
                // ---------------------------

                List<Person> people = new List<Person> { person };
                return SingleResult.Create(people.AsQueryable());
            }

            // -----------------------------
            // Otherwise, return a 404 error
            // -----------------------------

            throw new HttpResponseException(
                Request.CreateErrorResponse(HttpStatusCode.NotFound,
                String.Format("Could not retrieve Person with personId = {0}", key)));
        }

    }
}