﻿using System;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using Thinktecture.IdentityModel.Owin.ResourceAuthorization;
using Tyler.WebApp.Security.Extensions;
using Tyler.WebApp.Security.Web;

namespace TylerUITrainingWebApp.Security
{
    public class TylerUITrainingWebAppAuthorization : TylerAuthorizationManager
    {
        // -------------------------------------------------------------------------------------------
        // Entry method: all security checks triggered by [ResourceAuthorize] attributes start here...
        // -------------------------------------------------------------------------------------------
        public override Task<bool> CheckAccessAsync(ResourceAuthorizationContext context)
        {
            //return Ok();

            #region commented-out
            //// check claims and throw forbidden to deny access
            //// use ClaimsPrincipal Extensions to get to custom claims
            //var userId = context.Principal.GetOipUserID();
            //if (userId > 99999)
            //{
            //    throw new HttpResponseException(HttpStatusCode.Forbidden);
            //}

            //// check for system user if using Odyssey IDP and bypass all other security checks
            //int odysseyUserId = 0;
            //int.TryParse(context.Principal.GetClaim("http://schemas.xmlsoap.org/ws/2005/05/identity/claims/userid"), out odysseyUserId);
            //if (odysseyUserId == 1)
            //    return Ok();
            #endregion

            // ---------------------------------------------------------
            // User must be logged-in (or we wouldn't have decorated the
            // controller method with the [ResourceAuthorize] attribute)
            // ---------------------------------------------------------
            if (!context.Principal.Identity.IsAuthenticated)
            {
                return Nok();
            }

            // ---------------------------------------------------------
            // Perform resource/controller-specific security validations
            // ---------------------------------------------------------
            var resource = context.Resource();
            if (String.Compare(resource, TylerUITrainingWebAppResources.People, true) == 0)
            {
                return CheckPeopleAccessAsync(context);
            }

            // ------------------------
            // Unknown/invalid resource
            // ------------------------
            return Nok();
        }

        // ---------------------------------------------------
        // Perform general security checks for People resource
        // ---------------------------------------------------
        private Task<bool> CheckPeopleAccessAsync(ResourceAuthorizationContext context)
        {
            string action = context.Action();
            switch (action)
            {
                case TylerResources.Actions.ViewAll:
                    // -------------------------------------------
                    // List of People - High-level properties only
                    //   --> all authenticated users have access.
                    // -------------------------------------------
                    return Ok();

                case TylerResources.Actions.View:
                    // ---------------------------------------
                    // One Person - All properties
                    //   --> let's simulate row-level security
                    // ---------------------------------------
                    var key = String.Empty;
                    return (context.TryGetContextDataKey(out key)) ? CheckPeopleAccessByKeyAsync(context, key) : Nok();

                default:
                    // ---------------------------
                    // Unknown / unexpected action
                    // ---------------------------
                    return Nok();
            }
        }

        // -----------------------------------------------------
        // Perform row-level security checks for People resource
        // -----------------------------------------------------
        private Task<bool> CheckPeopleAccessByKeyAsync(ResourceAuthorizationContext context, string key)
        {
            if (key == "1")
            {
                return Eval("portal_admin@yourdomain.com".Equals(
                  context.Principal.Identity.Name, StringComparison.OrdinalIgnoreCase));
            }

            if (key == "5")
            {
                return Nok();
            }

            return Ok();
        }
    }
}