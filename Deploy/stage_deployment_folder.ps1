### Functions ###
function quit {
  param( $msg )
  write-host "`r`n$msg `r`nexiting..."
  $null = $Host.UI.RawUI.ReadKey('NoEcho,IncludeKeyDown');
  exit -1;
}

function cleanup {

  write-host "`r`nCleaning up..."
  # Process composition items for delete
  #$composeElts = $imsxml | Select-Xml -XPath 'software/buildIS/composePath'
  if ($composeElts.length -gt 0) {
    foreach($elt in $composeElts) {
      $source = $elt.Node.GetAttribute("source")
      $target = $elt.Node.GetAttribute("target")
      
      if (test-path "$source") {
        
        #remove target dir
        if (test-path "$target") {
          remove-item "$target" -recurse
          "  removing $target"
        }    
      }
    }
    
    #after cleanup, empty directories that were part of the composition process
    $folders = @()
    foreach ($root in $composeRoots) {
      if (-not (test-path $root)) { continue }
      
      foreach ($folder in (get-childitem -path $root -recurse | where { $_.PSisContainer -and $_.GetFileSystemInfos().Count -eq 0 })) {
        $folders += new-object PSObject -property @{
          object = $folder
          depth = ($folder.FullName.Split("\")).Count
        }
      }
      
      $rootFolder = get-item $root
      $folders += new-object PSObject -property @{
        object = $rootFolder
        depth = ($rootFolder.FullName.Split("\")).Count
      }
    }
    $folders = $folders | sort depth -descending | foreach ($_) { if ($_.object.GetFileSystemInfos().Count -eq 0) { remove-item $_.object.FullName } }
  }

  write-host -nonewline "..."
  write-host -foregroundcolor DarkGreen "done"

  write-host "`r`nRemoving IMS catalog index"

  # Delete catalog index
  if (test-path ims_root) 
    { remove-item ims_root -recurse } 

  # Delete IMS zip file
  if (test-path "$softwareName.zip") 
    { remove-item "$softwareName.zip" } 
}

function buildIMSZip {
  write-host ""
  BuildIS /Feature "$softwareName" /Version 0.0.0.0 /Label "$consoleLabel" /Output "$softwareName.zip" /InstallCat Ext .
}

function installPackage {
  write-host ""
  $path = Get-Location
  IMSInstall.exe /Path "$path" /Mode "IALL"
}

function installAppOnly {
  write-host ""
  $path = Get-Location
  IMSInstall.exe /Path "$path" /Mode "IAPP"
}


### Initial Execution ###

# Delete catalog index
if (test-path ims_root) 
  { remove-item ims_root -recurse } 

write-host "Starting composition script"

# Find .imsdef file
$files = @(Get-ChildItem . -recurse -include *.imsdef)
if ($files.length -gt 0) {
  $definitionFile = $files[0].fullname
}

# Validate imsdef file exists
if (-not $definitionFile)
  { quit("Unable to find a proper IMSDEF file"); }

if (-not (test-path $definitionFile)) 
  { quit("Unable to find file '$definitionFile'"); }
  
write-host "`r`nProcessing file $definitionFile"


# Move working directory to location of imsdef file
pushd
cd (split-path $definitionFile)

#load imsdef file
[xml]$imsxml = gc $definitionFile

$elt = $imsxml | Select-Xml -XPath 'software'
$softwareName = $elt.Node.GetAttribute("softwareName")
$consoleLabel = $elt.Node.GetAttribute("consoleLabel")
$consoleGroup = $elt.Node.GetAttribute("consoleGroup")
  
$composeElts = $imsxml | Select-Xml -XPath 'software/buildIS/composePath'
$composeRoots = @()

# build the catalog
write-host "`r`nSetting up IMS build catalog"
IMSCatalog.exe /Add $softwareName /Type Build /Family Odyssey /Parent Ext /Path .


#process composition items
if ($composeElts.length -gt 0) {
  write-host "`r`nComposing paths..."
  foreach($elt in $composeElts) {
    $source = $elt.Node.GetAttribute("source")
    $target = $elt.Node.GetAttribute("target")
    
    if (test-path "$source") {
      
      #clean target dir
      if (test-path "$target") {
        remove-item "$target" -recurse
      }
      
      #when copying a file, copy-item requires the path to exist first
      if (test-path "$source" -pathType leaf)
        { $null = new-item -itemtype File -path $target -force }

      copy-item "$source" "$target" -recurse
      "  $source => $target"
    }
    
    #collect top-level directories used during compose so we can clean them up later      
    $dirs = $target.Split("\")
    if ($dirs.Count -gt 1)
      { if ($composeRoots -notcontains $dirs[0]) { $composeRoots += $dirs[0] } }

  }
}

#return to working directory
popd

write-host -nonewline "..."
write-host -foregroundcolor DarkGreen "done"
write-host -foregroundcolor Cyan "`r`nDeployment folder is staged."

write-host -foregroundcolor Magenta "`r`n`r`n** Always publish your projects in Visual Studio before running this **`r`n"


$menuDone = $FALSE

while (-not $menuDone) {
  $caption = ""
  $message = "`r`nWhat would you like to do next?"
  $cleanup = new-Object System.Management.Automation.Host.ChoiceDescription "&Cleanup", "Cleanup"
  $build = new-Object System.Management.Automation.Host.ChoiceDescription "&Build IMS zip", "Build IMS zip file"
  $install = new-Object System.Management.Automation.Host.ChoiceDescription "&Install", "Install Both App and Database"
  $installA = new-Object System.Management.Automation.Host.ChoiceDescription "Install &App Only", "Install App Only"
  $choices = [System.Management.Automation.Host.ChoiceDescription[]]($cleanup, $build, $install, $installA)

  $answer = $host.UI.PromptForChoice($caption, $message, $choices, 0)

  switch ($answer)
  {
    0 { $menuDone = $TRUE }
    1 { buildIMSZip }
    2 { installPackage }
    3 { installAppOnly }
  }
}


cleanup



write-host -foregroundcolor Cyan "`r`n`r`nPress any key to exit"
$null = $Host.UI.RawUI.ReadKey('NoEcho,IncludeKeyDown')



