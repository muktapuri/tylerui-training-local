/* 
This file contains methods used by Web Apps to help with deployment.

Methods commonly called from IMSPKG
  * void applyPortalConfigToApp(filePath, uriPath)
      string filePath - application's path on disk
      string uriPath - application's uri path
      
  * void applyPublicIdpConfig(filePath, uriPath)
      string filePath - application's path on disk
      string uriPath - application's uri path
  
  * void applyOdysseyConfigToApp(filePath, uriPath)
      string filePath - application's path on disk
      string uriPath - application's uri path
  
  * xmlDocument loadWebConfig(path)
      string path - path to the application directory. Filename is assumed to be web.config
  
  * void saveWebConfig(webConfig, path)
      xmlDocument webConfig - the loaded web.config file
      string path - path to the application directory. Filename is assumed to be web.config
  
  * void addAppSetting(webConfig, key, value, [log])
      xmlDocument webConfig - the loaded web.config file
      string key - the setting's key
      string value - the setting's value
      boolean log - optional parameter, adds a trace message to the IMS console to report the app setting value. Default is true. 
  
  * void updateElmahConnectionString(webConfig, [connString])
      xmlDocument webConfig - the loaded web.config file
      string connString - optional parameter, the connection string to use. Default is the result of 'getConnectionString(getOdysseySiteId())'.
    
  * string getConnectionString(siteKey)

  * void addOmniBarAppSettingOdyssey(webConfig)
      webConfig - app web config file
      set the omni bar app and service endpoints for Odyssey apps

  * void addOmniBarAppSettingPortal(webConfig)
      webConfig - app web config file
      set the omni bar app and service endpoints for Portal apps

  * string getOdysseyBaseUri()
    returns "https://<odyssey cluster name>/"

  * string getPortalBaseUri() 
    returns "https://<portal cluster name>/"

- From Portal IMS Console Plugin -
  * string getOdysseySiteIdFromPortalConfig()
    
- From Elastic Search IMS Console Plugin -
  * string getElasticSearchUri()
  
- From Identity Provider IMS Console Plugin -
  * string getOdysseyIdpClusterName()
  
  * string getOdysseySiteId()
  
  
Including this utility file in your IMSPKG
  <asset source="webapputil.js" name="Ody.WebApp.HearingScheduling.webapputil.js" copy="false" />
  <include source="webapputil.js" />
  
  notes: 
    The asset tag is used so that IMS will include the .js file into your package for deployment. The include tag causes IMS
    to load the .js file at that point in the instruction set. All the code, methods and variable defined in the .js file will
    then be globally available. Only one ims file should have the asset tag. Any number of ims files can use the include.
  

Common examples of usage from an IMSPKG

  Applying identity configuration to web.config for a web application
    <script>
    <![CDATA[
      applyPortalConfigToApp(appPath, appUriPath);
    ]]>
    </script>
    
  Setting the siteId on web.config for a web service
    <script>
    <![CDATA[
      var webConfig = loadWebConfig(servicePath);
      addAppSetting(webConfig, "SiteID", getOdysseySiteIdFromPortalConfig());
      saveWebConfig(webConfig, servicePath); 
    ]]>
    </script>   
    
*/

// Global variables used for deployment
var appRootDir = "";
var apiRootDir = "";

// eventually omni settings (or base URIs) may need to be configured by realm
var omniAppPath = "/app/TylerUiOmniBar/";
var omniServiceAppPath = "/app/TylerUiOmniBarService/";

// Automatically setup the /app and /api virtual folders
setupWebAppBaseVirtualDirectories();



//
//
//
function addOmniBarAppSettingOdyssey(webConfig) {
  // for internal only, external use portal base uri
  // getOdysseyIdpClusterName fqdn from idp plugin
  addAppSetting(webConfig, "OmniBar:BaseUri", "https://" + getOdysseyIdpClusterName() + omniAppPath);
  addAppSetting(webConfig, "OmniBar:BaseUriService", "https://" + getOdysseyIdpClusterName() + omniServiceAppPath);
}
function addOmniBarAppSettingPortal(webConfig) {
  // for internal only, external use portal base uri
  var portalSite = getPortalSite();
  if (portalSite === null) {
    RaiseError(-1, "No valid Portal sites are configured. Please configure a portal site.", "", "", "");
  }
  var portalWebCluster = portalSite.getAttribute("portalwebcluster");
  // do we really need a fallback for portalWebCluster?
  addAppSetting(webConfig, "OmniBar:BaseUri", "https://" + portalWebCluster + omniAppPath);
  addAppSetting(webConfig, "OmniBar:BaseUriService", "https://" + portalWebCluster + omniServiceAppPath);
}

function getOdysseyBaseUri() {
  return "https://" + getOdysseyClusterName();
}
function getPortalBaseUri() {
  return "https://" + getPortalClusterName();
}
function applyPortalConfigToApp(filePath, uriPath) {
  var idpConfig = getPortalIdpConfig(uriPath);
  if (!!idpConfig) {
    applyIdpConfiguration(filePath, uriPath, idpConfig);
  }
  var webConfig = loadWebConfig(filePath);
  // set additional AppSettings
  addOmniBarAppSettingPortal(webConfig);
  //
  saveWebConfig(webConfig, filePath);
}
function applyPublicIdpConfig(filePath, uriPath) {
  RaiseError(-1, "applyPublicIdpConfig is not yet implemented.", "", "", "");
  var idpConfig = getPortalIdpConfig(uriPath);
  if (!!idpConfig) {
    applyIdpConfiguration(filePath, uriPath, idpConfig);
  }
}
function applyOdysseyConfigToApp(filePath, uriPath) {
  var idpConfig = getOdysseyIdpConfig(uriPath);
  if (!!idpConfig) {
    applyIdpConfiguration(filePath, uriPath, idpConfig);

    // additional app settings
    // turn off OIP Claims
    var webConfig = loadWebConfig(filePath);
    addAppSetting(webConfig, "RetrieveExtendedOipClaims", "false", false);
    addOmniBarAppSettingOdyssey(webConfig);
    saveWebConfig(webConfig, filePath);

    // add relying party to local idp config
    var siteId = getOdysseySiteId();
    addRelyingPartyToLocalIdp(siteId, idpConfig.audienceUri);
  }
}

function applyIdpConfiguration(filePath, uriPath, idpConfig) {
  if (idpConfig === null || idpConfig === undefined)
    RaiseError(-1, "applyIdentityConfiguration - idpConfig parameter must be supplied.", "", "", "");

  var webConfig = loadWebConfig(filePath);

  // App settings
  addAppSetting(webConfig, "ida:FederationMetadataLocation", idpConfig.metadataLoc, false);
  addAppSetting(webConfig, "ida:Realm", idpConfig.audienceUri, false);
  addAppSetting(webConfig, "ida:AudienceUri", idpConfig.audienceUri, false);
  addAppSetting(webConfig, "oipIntegrationApiUrl", idpConfig.oipApiUri, false);
  addAppSetting(webConfig, "applicationClientConfigurationId", idpConfig.clientConfigId, false);

  // Identity Configuration
  clearIdConfigAudienceUris(webConfig);
  addIdConfigAudienceUri(webConfig, idpConfig.audienceUri);

  updateIdConfigIssuerAuthorityName(webConfig, idpConfig.authorityName);

  clearIdConfigAuthorityThumbprints(webConfig);
  addIdConfigAuthorityThumbprint(webConfig, idpConfig.thumbprint);

  clearIdConfigValidIssuers(webConfig);
  addIdConfigValidIssuer(webConfig, idpConfig.validIssuer);


  // Federation Configuration
  updateWsFedConfigIssuer(webConfig, idpConfig.wsfedIssuer);
  updateWsFedConfigRealm(webConfig, idpConfig.wsfedRealm);
  if (!!idpConfig.wsfedReply && idpConfig.wsfedReply.length > 0)
    updateWsFedConfigReply(webConfig, idpConfig.wsfedReply);

  trace("Applied identity provider configuration", getIdpConfigDisplay(idpConfig));

  saveWebConfig(webConfig, filePath);
}



//
// IMS Plugin Config Methods
//

function getOdysseySiteIdFromPortalConfig() {
  var portalSite = getPortalSite();
  if (portalSite === null)
    RaiseError(-1, "No Portal Sites are configured. Please configure a portal site.", "", "", "");

  var siteName = portalSite.getAttribute("name");
  //log("portalSite", portalSite.xml);

  var portalAppSettings = portalSite.selectSingleNode("instructions/appSettings");
  var siteId = getAppSetting(portalAppSettings, "OdysseySiteID");
  if (siteId.length === 0)
    RaiseError(-1, "Unable to retrieve an Odysey SiteID from portal configuration '" + siteName + "'", "", "", "");

  return siteId;
}

function getPortalSite() {
  var portalSiteQuery = "eportalsites/eportalsite[instructions/appSettings/add[@key='LoginRegistrationDisabled' and @value='false']]";
  var ePortalSitesList = Realm.ConfigurationDOM.documentElement.selectNodes(portalSiteQuery);

  // for now, just return the first.. 
  return ePortalSitesList.length > 0 ? ePortalSitesList[0] : null;
}


// Elastic Search Plugin

function getElasticSearchUri() {
  var node = Realm.ConfigurationDOM.selectSingleNode('/propertyset/configurations/configuration[@name=\'elasticsearchconfig\']/@elasticsearchuri');

  return (!!node) ? node.text : "";
}

// IDP Plugin

function getOdysseyIdpClusterName() {
  var node = Realm.ConfigurationDOM.selectSingleNode('/propertyset/configurations/configuration[@name=\'idpconfig\']/@odyidpclustername');

  return (!!node) ? node.text : "";
}

function getOdysseyIdpSiteId() {
  var node = Realm.ConfigurationDOM.selectSingleNode('/propertyset/configurations/configuration[@name=\'idpconfig\']/@siteKey');

  return (!!node) ? node.text : "";
}

// Get Odyssey Site ID from IDP Plugin, gets the 'first' site if the plugin is not configured
function getOdysseySiteId() {
  var siteId = getOdysseyIdpSiteId();
  if (siteId.length === 0) {
    var elt = Realm.ConfigurationDOM.selectSingleNode('/propertyset/sites/site/@name');
    if (elt === null)
      RaiseError(-1, "Could find an Odyssey SiteID. An Odyssey site must be configured.", "", "", "");
    siteId = elt.text;
  }

  return siteId;
}



//
// Retrieve IDP Config methods
//

function getPortalIdpConfig(uriPath) {
  var idpConfig = {};

  // Odyssey config
  var odysseyWebCluster = getOdysseyClusterName();

  // Portal config
  var portalSite = getPortalSite();
  if (portalSite === null)
    RaiseError(-1, "No valid Portal sites are configured. Please configure a portal site.", "", "", "");

  var siteName = portalSite.getAttribute("name");
  var oipThumbprint = portalSite.getAttribute("oipserverthumbprint");
  var odySiteKey = portalSite.getAttribute("odysseysite");
  var portalWebCluster = portalSite.getAttribute("portalwebcluster");
  var appClientConfigId = portalSite.getAttribute("appclientconfigid");
  var oipServer = portalSite.getAttribute("oipserver");

  //log("portalSite", portalSite.xml);  

  var clusterName = portalWebCluster.length > 0 ? portalWebCluster : odysseyWebCluster;
  var oipApiUri = "https://" + oipServer + "/Tyler.OIP.IntegrationAPI/api/";
  var idpUri = "https://" + oipServer + "/idp";
  var metadataLoc = idpUri + "/FederationMetadata/2007-06/FederationMetadata.xml";
  var authorityUri = idpUri;
  var appUri = "https://" + clusterName + uriPath;


  idpConfig.source = "Portal (portal site '" + siteName + "')";

  idpConfig.metadataLoc = metadataLoc;
  idpConfig.oipApiUri = oipApiUri;
  idpConfig.clientConfigId = appClientConfigId;

  idpConfig.audienceUri = appUri;
  idpConfig.authorityName = authorityUri;
  idpConfig.thumbprint = oipThumbprint;
  idpConfig.validIssuer = authorityUri;

  idpConfig.wsfedIssuer = idpUri + "/issue/wsfed";
  idpConfig.wsfedRealm = appUri;
  idpConfig.wsfedReply = appUri;

  trace("Loaded Portal IDP configuration from portal site '" + siteName + "'");

  return idpConfig;
}

function getPublicIdpConfig(uriPath) {
  return null;
}

function getOdysseyIdpConfig(uriPath) {
  var idpConfig = {};

  //var clusterName = getOdysseyClusterName();
  var idpClusterName = getOdysseyIdpClusterName();
  var odyThumbprint = Realm.ConfigurationDOM.selectSingleNode('/propertyset/configurations/configuration[@name=\'idpconfig\']/@odyidpthumbprint');

  if (!odyThumbprint || idpClusterName.length === 0) {
    log("Odyssey IDP is not properly configured. Skipping IDP configuration.");
    return null;
  }

  var idpUri = "https://" + idpClusterName + "/idp";
  var metadataLoc = idpUri + "/FederationMetadata/2007-06/FederationMetadata.xml";
  var authorityUri = idpUri;
  var appUri = "https://" + idpClusterName + uriPath;


  idpConfig.source = "Odyssey";

  idpConfig.metadataLoc = metadataLoc;

  idpConfig.audienceUri = appUri;
  idpConfig.authorityName = authorityUri;
  idpConfig.thumbprint = odyThumbprint.text;
  idpConfig.validIssuer = authorityUri;

  idpConfig.wsfedIssuer = idpUri + "/issue/wsfed";
  idpConfig.wsfedRealm = appUri;

  trace("Loaded Odyssey IDP configuration");

  return idpConfig;
}



//
// IMS Core Config methods
//

function getOdysseyClusterName() {
  var clusterName = "";
  var clusterNode = Realm.ConfigurationDOM.selectSingleNode('/propertyset/configurations/configuration[@name=\'clustername\']/@clustername');

  if (!!clusterNode) {
    clusterName = clusterNode.text;
  }

  return clusterName.toLowerCase();
}

// Returns a list of all the Odyssey sites configured in the realm
function getOdysseySiteIds() {
  var siteIds = [];
  var elts = Realm.ConfigurationDOM.selectNodes('/propertyset/sites/site/@name');
  if (elts.length === 0)
    RaiseError(-1, "Could find an Odyssey SiteID. An Odyssey site must be configured.", "", "", "");

  for (var i = 0; i < elts.length; i++) {
    siteIds[i] = elts[i].text;
  }

  return siteIds;
}



//
// AppSettings methods
//

function getAppSetting(appSettings, keyName) {
  if (!!appSettings) {
    var key = appSettings.selectSingleNode("add[@key = '" + keyName + "']");
    if (!!key) {
      var value = key.getAttribute("value");
      return value;
    }
  }

  return "";
}

function addAppSetting(webConfig, key, value, log) {
  if (arguments.length < 4) log = true;
  updateAppSetting(webConfig, key, value, "add", log);
}
function removeAppSetting(webConfig, key, value, log) {
  if (arguments.length < 4) log = true;
  updateAppSetting(webConfig, key, value, "remove", log);
}
function clearAppSetting(webConfig, key, value, log) {
  if (arguments.length < 4) log = true;
  updateAppSetting(webConfig, key, value, "clear", log);
}

function updateAppSetting(webConfig, key, value, nodeName, log) {
  // allow for optional parameters
  if (arguments.length < 5) log = true;
  if (arguments.length < 4) nodeName = "add";

  if (!!webConfig && !!key) {
    // if supplied value is null, set it to empty string
    if (!value) {
      value = "";
    }

    var settingsNode = webConfig.selectSingleNode("/configuration/appSettings");
    if (!settingsNode) {
      RaiseError(-1, "'appSettings' node not found", "", "", "");
    }

    // remove nodes with the same key. keep the first node so we can do an in-place replacement if possible
    var nodes = webConfig.selectNodes("/configuration/appSettings/*[@key='" + key + "']");
    var replaceNode = null;
    if (nodes.length > 0) {
      replaceNode = nodes[0];
      for (var i = 1; i < nodes.length; i++)
        settingsNode.removeChild(nodes[i]);
    }

    var settingNode = webConfig.createElement(nodeName);
    if (replaceNode !== null) {
      settingsNode.replaceChild(settingNode, replaceNode);
    } else {
      settingsNode.appendChild(settingNode);
    }
    settingNode.setAttribute("key", key);
    settingNode.setAttribute("value", value);

    if (log) {
      trace("Applied appSetting: " + settingNode.xml);
    }

  }
}



//
// Shorthand Methods
//

function updateElmahConnectionString(webConfig, connString) {
  if (arguments.length < 2) {
    connString = getElmahConnectionString(getOdysseySiteId())
  }

  var path = "/configuration/connectionStrings/add[@name='Elmah.Sql']";
  safeSetAttribute(webConfig, path, "connectionString", connString);
}

function getElmahConnectionString(siteId) {
  var str = getConnectionString(siteId);

  // elmah doesn't support Provider option
  return str.replace("Provider=SQLOLEDB.1;", "");
}


//
// Identity Config shorthand methods
//

function updateIdConfigIssuerAuthorityName(webConfig, authorityName) {
  var path = "/configuration/system.identityModel/identityConfiguration/issuerNameRegistry/authority";
  safeSetAttribute(webConfig, path, "name", authorityName);
}

function clearIdConfigAudienceUris(webConfig) {
  var path = "/configuration/system.identityModel/identityConfiguration/audienceUris";
  clearChildren(webConfig, "/configuration/system.identityModel/identityConfiguration/audienceUris");
}

function addIdConfigAudienceUri(webConfig, audienceUri) {
  var path = "/configuration/system.identityModel/identityConfiguration/audienceUris";
  appendSimpleAddElt(webConfig, path, "value", audienceUri);
}


function clearIdConfigAuthorityThumbprints(webConfig) {
  var path = "/configuration/system.identityModel/identityConfiguration/issuerNameRegistry/authority/keys/*[@thumbprint]";
  if (webConfig === null) return;

  var elts = webConfig.selectNodes(path);
  for (var i = 0; i < elts.length; i++) {
    elts[i].parentNode.removeChild(elts[i]);
  }
}

function addIdConfigAuthorityThumbprint(webConfig, thumbprint) {
  var path = "/configuration/system.identityModel/identityConfiguration/issuerNameRegistry/authority/keys";
  appendSimpleAddElt(webConfig, path, "thumbprint", thumbprint);
}


function clearIdConfigValidIssuers(webConfig) {
  var path = "/configuration/system.identityModel/identityConfiguration/issuerNameRegistry/authority/validIssuers";
  clearChildren(webConfig, path);
}

function addIdConfigValidIssuer(webConfig, issuer) {
  var path = "/configuration/system.identityModel/identityConfiguration/issuerNameRegistry/authority/validIssuers";
  appendSimpleAddElt(webConfig, path, "name", issuer);
}

// Federation Configuration

function updateWsFedConfigIssuer(webConfig, issuer) {
  var path = "/configuration/system.identityModel.services/federationConfiguration/wsFederation";
  safeSetAttribute(webConfig, path, "issuer", issuer);
}

function updateWsFedConfigRealm(webConfig, realm) {
  var path = "/configuration/system.identityModel.services/federationConfiguration/wsFederation";
  safeSetAttribute(webConfig, path, "realm", realm);
}

function updateWsFedConfigReply(webConfig, reply) {
  var path = "/configuration/system.identityModel.services/federationConfiguration/wsFederation";
  safeSetAttribute(webConfig, path, "reply", reply);
}



//
// XML helper methods
//

function clearChildren(doc, path) {
  if (doc === null) return;
  var elt = doc.selectSingleNode(path);
  if (elt === null) return;

  while (elt.firstChild)
    elt.removeChild(elt.firstChild);
}

function appendSimpleAddElt(doc, parentPath, attrName, attrValue) {
  if (doc === null) return;
  var parent = doc.selectSingleNode(parentPath);
  if (parent === null) return;

  var existingElt = parent.selectSingleNode("add[@" + attrName + "='" + attrValue + "']");
  if (existingElt !== null) return;

  var elt = parent.ownerDocument.createElement("add");
  if (elt === null) return;
  elt.setAttribute(attrName, attrValue);

  parent.appendChild(elt);
}

function safeSetAttribute(doc, path, attribute, value) {
  if (doc === null) return false;
  var elt = doc.selectSingleNode(path);
  if (elt === null) return false;
  elt.setAttribute(attribute, value);

  return true;
}



//
// Initial setup methods
//

function setupWebAppBaseVirtualDirectories() {
  appRootDir = CurrentPackage.MapTarget('\\Webs\\app');
  apiRootDir = CurrentPackage.MapTarget('\\Webs\\api');

  setupEmptyVirtualDir(appRootDir, "/app");
  setupEmptyVirtualDir(apiRootDir, "/api");
}

function setupEmptyVirtualDir(physicalPath, virtualPath) {
  if (!Storage.ObjectExists(physicalPath)) {
    Storage.CreateDirectory(physicalPath);
    trace("created path '" + physicalPath + "'");
  }
  WebAdministrator.CreateVirtualDirectory(WebSite, virtualPath, physicalPath, "", Identity, Password, 7);
}



//
//
//

function loadWebConfig(appPath) {
  var webConfig = loadXmlFile(appPath, "Web.config");

  return webConfig;
}

function saveWebConfig(webConfig, appPath) {
  saveXmlFile(webConfig, appPath, "Web.config");
}


function loadXmlFile(path, name) {
  var fileName = Storage.ComposePath(path, name);
  if (!Storage.ObjectExists(fileName)) {
    RaiseError(-1, "Unable to find file", "", "", "Expected location: " + fileName);
  }

  var file = LoadXML(fileName);

  return file;
}

function saveXmlFile(file, path, name) {
  var fileName = Storage.ComposePath(path, name);

  file.save(fileName);
}


function log(msg, clipboard) {
  if (arguments.length < 2) clipboard = "";
  LogMessage(2, 0, msg, "", "", clipboard);
}

function trace(msg, clipboard) {
  if (arguments.length < 2) clipboard = "";
  LogMessage(4, 0, msg, "", "", clipboard);
}

function stripTrailingSlash(str) {
  return str.replace(/\/$/, "");
}

function getIdpConfigDisplay(idpConfig) {
  var msg = "";
  msg += "Source: " + idpConfig.source + "\r\n";
  msg += "\r\n";
  msg += "oip Api Uri:   " + idpConfig.oipApiUri + "\r\n";
  msg += "clientConfigId:   " + idpConfig.clientConfigId + "\r\n";
  msg += "\r\n";
  msg += "idconfig Audience Uri:   " + idpConfig.audienceUri + "\r\n";
  msg += "idconfig Authority Name: " + idpConfig.authorityName + "\r\n";
  msg += "idconfig Thumbprint:     " + idpConfig.thumbprint + "\r\n";
  msg += "idconfig Valid Issuer:   " + idpConfig.validIssuer + "\r\n";
  msg += "\r\n";
  msg += "wsfed Issuer: " + idpConfig.wsfedIssuer + "\r\n";
  msg += "wsfed Realm:  " + idpConfig.wsfedRealm + "\r\n";
  if (!!idpConfig.wsfedReply && idpConfig.wsfedReply.length > 0)
    msg += "wsfed Reply:  " + idpConfig.wsfedReply + "\r\n";

  return msg;

}


//
// IDP Database Methods
//

function addRelyingPartyToLocalIdp(siteKey, appUri) {
  // only execute this code on the orchestration server
  if (Server.ServerID != Realm.OrchestrationServerID)
    return;

  var connectionString = getConnectionString(siteKey);
  if (connectionString.length == 0) {
    trace("Unable to determine connection string. Relying Party configuration skipped.");
    return;
  }

  var dbConnection = new ActiveXObject("ADODB.Connection");
  try {
    dbConnection.Open(connectionString);
    var sqlText = "IF EXISTS(SELECT 1 FROM ThinkTectureIdentityProviderConfig.dbo.RelyingParties where Name = '" + appUri + "')\r\n" +
            "  SELECT 0 AS 'Updated'\r\n" +
            "ELSE\r\n" +
            "  BEGIN\r\n" +
            "    SET NOCOUNT ON\r\n" +
            "    INSERT INTO ThinkTectureIdentityProviderConfig.dbo.RelyingParties (Name, Enabled, Realm, TokenLifeTime) VALUES (?, 1, ?, 0)\r\n" +
            "    SELECT 1 AS 'Updated'\r\n" +
            "  END";

    var sqlParameters = [appUri, appUri];
    var isUpdated = executeSQLCommand(dbConnection, sqlText, sqlParameters);
    if (isUpdated == 0) {
      trace("Relying party '" + appUri + "' already exists in the local IDP configuration.");
    } else if (isUpdated == 1) {
      log("Relying party '" + appUri + "' added to the local IDP configuration.");
    }
  }
  catch (error) {
    // log exceptions as errors, but continue processing
    LogMessage(1, 0, "An error occurred while configuring the identity server.  See the exception details for more information.", "", "", error.message);
  }
  finally {
    dbConnection.Close();
    dbConnection = null;
  }
}

function executeSQLCommand(dbConnection, sqlText, sqlParameters) {
  var dbCommand = new ActiveXObject("ADODB.Command");

  dbCommand.ActiveConnection = dbConnection;
  dbCommand.CommandText = sqlText;
  dbCommand.CommandType = 1;

  var dbResult = dbCommand.execute(null, sqlParameters);

  var updatedValue = -1;
  if (!dbResult.BOF || !dbResult.EOF) {
    updatedValue = dbResult.Fields("Updated").Value;
  }
  return updatedValue;
}

function getConnectionString(siteKey) {
  // find the site
  var site = Realm.ConfigurationDOM.selectSingleNode("/propertyset/sites/site[@name='" + siteKey + "']");
  if (!site) {
    trace("No site was found for site key '" + siteKey + "'. Relying Party configuration skipped.");
    return "";
  }

  // find the database connection 
  var instanceID = site.getAttribute("instanceid");
  var connectionElement = site.selectSingleNode("configurations/configuration[@name='databaseconnections']");
  if (!connectionElement) {
    trace("No connection information was found for site '" + siteKey + "'. Relying Party configuration skipped.");
    return "";
  }


  // derive connection string from Operations configuration
  var authentication = connectionElement.getAttribute("authenticationtypeprimary");
  var operationsElement = connectionElement.selectSingleNode("databaseconnection[@name='Operations']");
  if (!operationsElement) {
    trace("Database connection configuration for Operations database not found for site '" + siteKey + "'. Relying Party configuration skipped.");
    return "";
  }

  var dataSource = operationsElement.getAttribute("datasource");
  var databaseName = operationsElement.getAttribute("database");
  var databaseLogin = operationsElement.getAttribute("user");
  var databasePassword = operationsElement.getAttribute("password");
  if (dataSource.length == 0) {
    trace("Database connection configuration for Operations database is invalid for site '" + siteKey + "'. Relying Party configuration skipped.");
    return "";
  }


  // locate datase instance
  var instance = null;
  if (instanceID != null && instanceID.length > 0) {
    instance = Instances.Item(instanceID);
  }
  else if (Instances.Count == 1) {
    instance = Instances.Item(0);
  }
  if (instance == null) {
    trace("No enabled instances found for site '" + siteKey + "'. Relying Party configuration skipped.");
    return "";
  }


  // determine connection string
  if (authentication == "SQL") {
    connectionString = "Provider=SQLOLEDB.1;Data Source=" + dataSource + ";User ID=" + databaseLogin + ";Password=" + databasePassword + ";Database=" + databaseName;
  }
  else {
    connectionString = "Provider=SQLOLEDB.1;Data Source=" + dataSource + ";Database=" + databaseName + ";Integrated Security=SSPI";
  }

  return connectionString;
}

